package ins.franjupucrack.franju.classroommanagement_v1;


import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;


public class Guardar {   //CLASSE PER ADMINISTRAR ELS METODES I LA INFO A GUARDAR
                                                                                        //PERQUE AQUESTA CLASSE SIGUI UTIL I EFICIENT S'HA DE CREAR
                                                                                        //METODES ESPECIFICS PER EDITAR I OBTENIR ELS VALORS DE LARRAY
    public static ArrayList<DadesAula> info=new ArrayList<DadesAula>();

    Context c;

    public Guardar(Context context){
        c=context;

    }


    public void guardarDades(){  //METODE PER GUARDAR L'ARRAY A LA MEMORIA DEL TELÈFON

        OutputStreamWriter out;

        try{


            out=new OutputStreamWriter(c.openFileOutput("dades",0));


            String jsonObjetos = new Gson().toJson(info);
            //SharedPreferences.Editor editor=prefs.edit();
            // editor.putString("b",jsonObjetos);//jsonObjetos

            out.write(jsonObjetos);
            out.flush();
            out.close();
            Toast.makeText(c,"Guardant...",Toast.LENGTH_SHORT).show();
            //editor.clear(); ///UTILITZAR?
            //editor.apply();

        }catch (Exception e){
            Toast.makeText(c,e.getMessage(),Toast.LENGTH_LONG).show();

        }

    }


    public void recuperarDades(){ //METODE PER RECUPERAR L'ARRAY GUARDADA A LA MEMORIA DEL TELEFON


        InputStreamReader in;

        try{
            in=new InputStreamReader(c.openFileInput("dades"));
            BufferedReader bufferedReader=new BufferedReader(in);
            StringBuffer inform= new StringBuffer();
            String tmp=null;
            while((tmp=bufferedReader.readLine())!=null){
                inform.append(tmp);
            }
            in.close();
            //  SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
            Gson gson=new Gson();
            //String objectes = prefs.getString("b","");
            //String json = new Gson().toJson(objectes);
            JSONArray jsonArray = new JSONArray(inform.toString()); //MIrar si es pot sense aquesta linia
            Type listType = new TypeToken<ArrayList<DadesAula>>(){}.getType(); //crec que la versio importada de Type no és la correcta
            this.info=new Gson().fromJson(jsonArray.toString(),listType); //json



            //TextView tv=(TextView)findViewById(R.id.textQueMostraLlistaBuida);


        }catch (Exception e){
            Toast.makeText(c,e.getMessage(),Toast.LENGTH_LONG).show();

        }

    }



}
