package ins.franjupucrack.franju.classroommanagement_v1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.franju.classroommanagement_v1.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;


public class ContactarAlumne extends Activity {


    EditText correu;
    EditText correuPares;
    String nomAlumne;
    String nomAula;
    ArrayList<DadesAula> dades;


    @Override
    public void onCreate(Bundle savedInstasceState){
        super.onCreate(savedInstasceState);

        setContentView(R.layout.contactar_alumne);

        nomAula=getIntent().getExtras().getString("nomAula");

        nomAlumne=getIntent().getExtras().getString("nomAlumne");

        ////////////////////
        obtenirDadesAlumne();
        ////////////////////

        correu=findViewById(R.id.editTextCorreu);
        correuPares=findViewById(R.id.editTextCorreuPares);

        /////////////////////
        informacioGuardada();
        /////////////////////


        Button b=findViewById(R.id.botoGuardarDireccio);
        b.setOnClickListener(new View.OnClickListener() {    //boto per guardar la direccio
            @Override
            public void onClick(View view) {
                guardarCorreu();

            }
        });

        Button b2=findViewById(R.id.botoEnviarCorreu);
        b2.setOnClickListener(new View.OnClickListener() {    //boto per enviar correu
            @Override
            public void onClick(View view) {
                enviarCorreu();

            }
        });

        Button b3=findViewById(R.id.botoGuardarDireccioPares);
        b3.setOnClickListener(new View.OnClickListener() {    //boto per guardar la direccio
            @Override
            public void onClick(View view) {
                guardarCorreuPares();

            }
        });

        Button b4=findViewById(R.id.botoEnviarCorreuPares);
        b4.setOnClickListener(new View.OnClickListener() {    //boto per enviar correu
            @Override
            public void onClick(View view) {
                enviarCorreuPares();

            }
        });

    }



    public void guardarDades(){

        OutputStreamWriter out;

        try{


            //dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes = a.notes;
            out=new OutputStreamWriter(openFileOutput("dades",0));


            String jsonObjetos = new Gson().toJson(dades);


            out.write(jsonObjetos);
            out.flush();
            out.close();
            Toast.makeText(this,"Guardant...",Toast.LENGTH_SHORT).show();


        }catch (Exception e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }

    }


    public void obtenirDadesAlumne(){ //METODE PER RECUPERAR L'ARRAY GUARDADA A LA MEMORIA DEL TELEFON

        DadesAlumne al=null;
        InputStreamReader in;

        try{
            in=new InputStreamReader(openFileInput("dades"));
            BufferedReader bufferedReader=new BufferedReader(in);
            StringBuffer info= new StringBuffer();
            String tmp=null;
            while((tmp=bufferedReader.readLine())!=null){
                info.append(tmp);
            }
            in.close();
            //  SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
            Gson gson=new Gson();
            //String objectes = prefs.getString("b","");
            //String json = new Gson().toJson(objectes);
            JSONArray jsonArray = new JSONArray(info.toString()); //MIrar si es pot sense aquesta linia
            Type listType = new TypeToken<ArrayList<DadesAula>>(){}.getType(); //crec que la versio importada de Type no és la correcta
            dades=new Gson().fromJson(jsonArray.toString(),listType); //json






        }
        catch (Exception e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }



    }


    public void enviarCorreu(){                   //METODE PER ENVIAR UN CORREU A LALUMNE UTILITZANT UN INTENT

        Intent i=new Intent();

        //acabar


    }
    public void enviarCorreuPares(){                   //METODE PER ENVIAR UN CORREU ALS PARES UTILITZANT UN INTENT

        Intent i=new Intent();

        //acabar


    }














    public int obtenirClasse(){
        int g=0;
        for(int b=0;b<dades.size();b++){
            if(dades.get(b).name.equals(nomAula)){
                g=b;

            }
        }
        return g;


    }


    public void informacioGuardada(){



        correu.setText(dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).correu);
        correuPares.setText(dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).correuPares);


    }

    public void guardarCorreu(){

        dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).correu= correu.getText().toString();

        guardarDades();
    }

    public void guardarCorreuPares(){

        dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).correuPares= correuPares.getText().toString();

        guardarDades();
    }

    public int obtenirAlumne(int g){
        int t=0;
        for(int k=0;k<dades.get(g).alumnes.size();k++){
            if(dades.get(g).alumnes.get(k).nom.equals(nomAlumne)){
                t=k;   //MIRAR

            }
        }

        return t;
    }


}
