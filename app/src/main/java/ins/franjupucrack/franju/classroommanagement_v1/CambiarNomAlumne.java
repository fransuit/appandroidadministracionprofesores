package ins.franjupucrack.franju.classroommanagement_v1;


import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franju.classroommanagement_v1.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class CambiarNomAlumne extends Activity{

    ArrayList<DadesAula> dades=new ArrayList<>();
    String nomAula;
    String nomAlumne;
    EditText e;
    Button b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

            setContentView(R.layout.cambiar_nom_alumne);

        recuperarDades();

        nomAula=getIntent().getExtras().getString("nomAula");
        nomAlumne=getIntent().getExtras().getString("nomAlumne");


        e=findViewById(R.id.editTextNouNomAlumne);


        b=findViewById(R.id.botoGuardarNouNomAlumne);


        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cambiar();
            }
        });



    }

    public void cambiar(){
        dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).nom=e.getText().toString();
        guardarDades();
        setResult(RESULT_OK);
        finish();
    }







    public int obtenirClasse(){
        int g=0;
        for(int b=0;b<dades.size();b++){
            if(dades.get(b).name.equals(nomAula)){
                g=b;

            }
        }
        return g;


    }
    public int obtenirAlumne(int g){
        int t=0;
        for(int k=0;k<dades.get(g).alumnes.size();k++){
            if(dades.get(g).alumnes.get(k).nom.equals(nomAlumne)){
                t=k;   //MIRAR

            }
        }

        return t;
    }



    public void guardarDades(){  //METODE PER GUARDAR L'ARRAY A LA MEMORIA DEL TELÈFON

        OutputStreamWriter out;

        try{
            // SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);

            out=new OutputStreamWriter(openFileOutput("dades",0));


            String jsonObjetos = new Gson().toJson(dades);
            //SharedPreferences.Editor editor=prefs.edit();
            // editor.putString("b",jsonObjetos);//jsonObjetos

            out.write(jsonObjetos);
            out.flush();
            out.close();
            Toast.makeText(this,"Guardant...",Toast.LENGTH_SHORT).show();
            //editor.clear(); ///UTILITZAR?
            //editor.apply();

        }catch (Exception e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }

    }


    public void recuperarDades(){ //METODE PER RECUPERAR L'ARRAY GUARDADA A LA MEMORIA DEL TELEFON


        InputStreamReader in;

        try{
            in=new InputStreamReader(openFileInput("dades"));
            BufferedReader bufferedReader=new BufferedReader(in);
            StringBuffer info= new StringBuffer();
            String tmp=null;
            while((tmp=bufferedReader.readLine())!=null){
                info.append(tmp);
            }
            in.close();
            //  SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
            Gson gson=new Gson();
            //String objectes = prefs.getString("b","");
            //String json = new Gson().toJson(objectes);
            JSONArray jsonArray = new JSONArray(info.toString()); //MIrar si es pot sense aquesta linia
            Type listType = new TypeToken<ArrayList<DadesAula>>(){}.getType(); //crec que la versio importada de Type no és la correcta
            dades=new Gson().fromJson(jsonArray.toString(),listType); //json



            TextView tv=(TextView)findViewById(R.id.textQueMostraLlistaBuida);


        }catch (Exception e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }

    }
}
