package ins.franjupucrack.franju.classroommanagement_v1;


import java.io.Serializable;
import java.util.ArrayList;

public class DadesAula implements Serializable {  //clase utilitzada per administrar les dades de les aules mitjançant un array

    public String name="classe predeterminada";

    ArrayList<NotesAlumne> notes=new ArrayList<NotesAlumne>();  //array per tenir controlades que tots els alumnes tinguin els mateixos materials d'evaluació

    ArrayList<DadesAlumne> alumnes=new ArrayList<DadesAlumne>();  //array que conté els alumnes

    public int minimFallosMemori=-1;


    public DadesAula(String name,ArrayList<DadesAlumne> alumnes,ArrayList<NotesAlumne> notesAE){  //constructor
          this.name=name;
          this.notes=notesAE;
          this.alumnes=alumnes;
          for(int i=0;i<alumnes.size();i++){
              alumnes.get(i).notes=notesAE; //MIRAR
          }
    }













}
