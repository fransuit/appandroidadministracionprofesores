package ins.franjupucrack.franju.classroommanagement_v1;


import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.AppCompatButton;


public class BotoPasarLlista extends AppCompatButton {
    //R.drawable

   public boolean clicat=false;

   public int tipus;
    // 1->assistencia,2->retard,3->falta

    public BotoPasarLlista(Context c, int t){
        super(c);

        this.tipus=t;
    }


    public void cambiarEstat(){  //crec que no serà necesari

      if(!clicat){
          clicat=true;

      }else{
          clicat=false;
      }

    }




    public int fons(){
        int a=0;
        if(tipus==1){
           if(!clicat){
               a=Color.parseColor("#90EE90");
           }else{
               a=Color.parseColor("#008000");
           }

        }if(tipus==2){
            if(!clicat){
                a=Color.parseColor("#FAC96F");
            }else{
                a=Color.parseColor("#DD8F00");
            }

        }if(tipus==3){
            if(!clicat){

                a=Color.parseColor("#F9A2A2");
            }else{
                a=Color.parseColor("#FF0000");
            }

        }
        return a;
    }



}
