package ins.franjupucrack.franju.classroommanagement_v1;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franju.classroommanagement_v1.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Classroom extends Activity{      //AFEGUIR UNA FOTO A LA LLISTA PER CADA ALUMNE
   // DadesAula dadesAula=null;
    public ArrayList<DadesAula> dades=new ArrayList<DadesAula>();
    String nomAula;

    final int MODIFICARALUMNE=3;
    final int  NOUALUMNE=5;
    final int AFEGUIRAE=1334;
    final int ELIMINARAE=45346;
    final int PASARLLISTA=75642;
    final int CAMBIDENOMALUMNE=7254;
    @Override
    public void onCreate(Bundle savedInstasceState){
        super.onCreate(savedInstasceState);

        setContentView(R.layout.classroom);

        ////////////////
        recuperarDades();
        ////////////////




        nomAula=(String)getIntent().getStringExtra("nomDeLaula");



        TextView t=(TextView)findViewById(R.id.textViewPerSaberQuinaAulaEstas);
        t.append(nomAula+":");


        FloatingActionButton options=findViewById(R.id.buttonMenuClasse);
        options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerForContextMenu(view);
                openContextMenu(view);
            }
        });


/*
        Button b=(Button)findViewById(R.id.addStudient);         //ARA ES FA AMB EL CONTEXTMENU
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                  afeguirAlumne();
            }
        });
*/



        ListView listView = (ListView) findViewById(R.id.LlistaAlumnes);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){                               //linies de codi per interectuar amb el pulsament d'un element de la llista
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){

                ValorsLlista info=(ValorsLlista) parent.getItemAtPosition(position);
                String nom=info.nomAlumne;
                //toast();
                intentVisualitzarAlumne(nom);      //metode per cambiar d'aquesta activity i anar a l'activity de l'alumne




            }
        });



        //dades=(ArrayList<DadesAula>)getIntent().getSerializableExtra("totesLesDades");     //CREC QUE AQUI HI HA ERROR

        ////////////////////
        posarDadesALaLlista();
        ////////////////////
        View v=(View)findViewById(R.id.LlistaAlumnes);   //MIRAR              //és el codig per fer que el context menu sobri quan es clica a un element de la llista
        registerForContextMenu(v);

    }





    /////////////////////////////////////////////////////////////////////////// METODES

    public void intentVisualitzarAlumne(String n){
        //guardarCanvis();
        Intent i=new Intent(this,Student.class);
        i.putExtra("nom",n);
        i.putExtra("nomAula",nomAula);
        startActivityForResult(i,MODIFICARALUMNE);
    }



    public void afeguirAlumne(){

        Intent i=new Intent(this,AsistentAfegirAlumnes.class);
        i.putExtra("id",1);
        i.putExtra("nomAula",nomAula);
        startActivityForResult(i,NOUALUMNE);

    }









    public void posarDadesALaLlista() {   //metode per fer que la llista de les classes


        ArrayList<ValorsLlista> resultList = new ArrayList<ValorsLlista>();

        for(int i=0;i<dades.get(obtenirClasse()).alumnes.size();i++) {                  //AQUEST BUCLE OMPLE LES DADES DE LARRAY RESULTLIST

            ValorsLlista vl = new ValorsLlista();
            vl.nomAlumne = dades.get(obtenirClasse()).alumnes.get(i).nom;               //afegir més coses si es vol afeguir coses als layouts de la llista
            resultList.add(vl);

        }

        Adaptadorllista adaptador = new Adaptadorllista(this, R.layout.layout_per_list_adapter_alumnes, resultList, getLayoutInflater());

        ListView listView = (ListView) findViewById(R.id.LlistaAlumnes);
        listView.setAdapter(adaptador);                 //ADAPTADOR DE LA LLISTA

        if (dades.get(obtenirClasse()).alumnes.isEmpty()) {

            TextView tv = (TextView) findViewById(R.id.textViewPerDirQueNoHiHaAlumnes);
            tv.setText("NO hi han alumnes assignats");

        } else {
            TextView tv = (TextView) findViewById(R.id.textViewPerDirQueNoHiHaAlumnes);
            tv.setText("");                                                             //MIRAR SI ES MILLOR POSAR UN HIDE
        }



    }

    public void eliminarAlumne(String n){
        int k=0;

        for(int h=0;h<dades.size();h++){            //BUCLE PER OBTENIR EL INDEX DE LA LLISTA CORRESPONENT
            if(dades.get(h).name.equals(nomAula)){
                k=h;
            }
        }

        for(int i=0;i<dades.get(k).alumnes.size();i++){
            if(dades.get(k).alumnes.get(i).nom.equals(n)){

                dades.get(k).alumnes.remove(i);
            }

        }
       // Toast.makeText(this,""+dades.get(k).alumnes.get(0).notes.get(0).valor,Toast.LENGTH_LONG).show();
        guardarCanvis(); //potser s'auria de treure aixo
        posarDadesALaLlista();

    }

    public void recuperarDades(){ //METODE PER RECUPERAR L'ARRAY GUARDADA A LA MEMORIA DEL TELEFON


        InputStreamReader in;

        try{
            in=new InputStreamReader(openFileInput("dades"));
            BufferedReader bufferedReader=new BufferedReader(in);
            StringBuffer info= new StringBuffer();
            String tmp=null;
            while((tmp=bufferedReader.readLine())!=null){
                info.append(tmp);
            }
            in.close();
            //  SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
            Gson gson=new Gson();
            //String objectes = prefs.getString("b","");
            //String json = new Gson().toJson(objectes);
            JSONArray jsonArray = new JSONArray(info.toString()); //MIrar si es pot sense aquesta linia
            Type listType = new TypeToken<ArrayList<DadesAula>>(){}.getType(); //crec que la versio importada de Type no és la correcta
            dades=new Gson().fromJson(jsonArray.toString(),listType); //json






        }catch (Exception e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }

    }
public void guardarCanvis(){

    OutputStreamWriter out;

    try{


        out=new OutputStreamWriter(openFileOutput("dades",0));


        String jsonObjetos = new Gson().toJson(dades);

        out.write(jsonObjetos);
        out.flush();
        out.close();
        Toast.makeText(this,"Guardant...",Toast.LENGTH_SHORT).show();


    }catch (Exception e){
        Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

    }
}


    public void afeguirAE(){
       Intent i=new Intent(this,AfeguirAE.class);

       i.putExtra("nomAula",nomAula);
       startActivityForResult(i,AFEGUIRAE);


    }







    public DadesAula obtenirUnaClasseDaterminada(String n){   //NO S'HA DUTILITZAR
        DadesAula d=null;
        for(int a=0;a<dades.size();a++){
            if(dades.get(a).name.equals(n)){
                d=dades.get(a);
            }
        }
        return d;

    }


    public int obtenirClasse(){
        int g=0;
        for(int b=0;b<dades.size();b++){
            if(dades.get(b).name.equals(nomAula)){
                g=b;

            }
        }
        return g;


    }
    /////////////////////////////////////////////////////////////////////////// METODES




    /////////////////////////////////////////////////////////////////////////  CLASSES LOCALS QUE HEREDEN

    public static class ValorsLlista{ //classe per insertar els valors al contructor de l'array adapter

        public String nomAlumne;                       //afeguir mes parametres si més endevant es necesiten


    }



    public static class Adaptadorllista extends ArrayAdapter {               //classe que hereda de arrayAdapter i que permet que la listview mostri el contingut d'una array
        private LayoutInflater mInflater;
        private List<ValorsLlista> mObjects;

        public Adaptadorllista(Context context, int resource, List<ValorsLlista> objects, LayoutInflater inflater){                    //FER AIXOO
            super(context,resource,objects);
            mInflater=inflater;
            mObjects=objects;

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            ValorsLlista valorsLlista=mObjects.get(position);
            View row=mInflater.inflate(R.layout.layout_per_list_adapter_alumnes,null,false);

            TextView tv=(TextView) row.findViewById(R.id.textPerLayoutDeArrayAdapterNomAlumne);
            tv.setText(valorsLlista.nomAlumne); //se li assigna el nom de l'aula al text view que es mostrara a la llista d'aules

            return row;

        }


    }
/////////////////////////////////////////////////////////////////////////  CLASSES LOCALS QUE HEREDEN


    ///////////////////////////////////////////////////////////////////////// METODES SOBREESCRITS
   @Override
   public void onActivityResult(int requestCode,int resultCode, Intent i){

        if(requestCode==NOUALUMNE){

            if(resultCode==RESULT_OK){
                dades.get(obtenirClasse()).alumnes.addAll((ArrayList<DadesAlumne>)i.getSerializableExtra("alumnes"));
                guardarCanvis();
                posarDadesALaLlista();
            }
            if(resultCode==RESULT_CANCELED){
                recuperarDades();  //MIRAR
            }

        }if(requestCode==MODIFICARALUMNE){
           if(resultCode==RESULT_CANCELED){
               recuperarDades();  //MIRAR
           }

       }
       if(requestCode==AFEGUIRAE){
            if(resultCode==RESULT_OK){
                //FER CODIG PER OBTENIR LES NOVES AE CREADES I AFEGUIRLES A LA AULA I PER CADA ALUMNME
                ArrayList<NotesAlumne> n=(ArrayList<NotesAlumne>)i.getSerializableExtra("notes");
                dades.get(obtenirClasse()).notes.addAll(n);
                for(int g=0;g<dades.get(obtenirClasse()).alumnes.size();g++){

                    dades.get(obtenirClasse()).alumnes.get(g).notes.addAll(n);  //MIRAR
                }

                guardarCanvis();
                posarDadesALaLlista();//no fa falta

            }
       }

       if(requestCode==ELIMINARAE){
           if(resultCode==RESULT_OK){
               recuperarDades();

           }

       }

       if(requestCode==PASARLLISTA){
           if(resultCode==RESULT_OK){
               recuperarDades();
           }
       }
       if(requestCode==CAMBIDENOMALUMNE){
           if(resultCode==RESULT_OK){
               recuperarDades();
               posarDadesALaLlista();
           }
       }

   }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){  //menu per cuan mantens pulsat un element de la llista
        super.onCreateContextMenu(menu,v,menuInfo);
        MenuInflater inflater;
        switch (v.getId()){
            case R.id.LlistaAlumnes:
                inflater=getMenuInflater();
                inflater.inflate(R.menu.clicar_element_llista_alumnes,menu);

                break;
            case R.id.buttonMenuClasse:
                inflater=getMenuInflater();
                inflater.inflate(R.menu.menu_aula,menu);

                 break;
        }


    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        AdapterView.AdapterContextMenuInfo delw=(AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        //mLastRowSelected=delw.position; -> mirar si es necesari


        switch (item.getItemId()){
            case R.id.eliminarAlumneMenuLlista:
                final String n=dades.get(obtenirClasse()).alumnes.get(delw.position).nom; //MIRAR , OBTENIR EL NOM DE LAULA PER SABER DE QUINA ES TRACTA, MIRAR SI ES MILLOR UTILITZAR UN ID
                new AlertDialog.Builder(this).setTitle("segur que vols eliminar l'Alumne?").setPositiveButton("si",new AlertDialog.OnClickListener(){

                    public void onClick(DialogInterface dg, int i){
                        //toast();
                        eliminarAlumne(n);
                    }
                }).setNegativeButton("no",null).show();
                return true;
            case R.id.cambiarNomAlumneMenuLlista:
                final String f=dades.get(obtenirClasse()).alumnes.get(delw.position).nom;
                   cambiarNomAlumne(f);

                  return true;
            case R.id.opcioPassarLlista:
                pasarllista();
                return true;
            case R.id.opcioAfeguirAlumne:
                afeguirAlumne();
                return true;
            case R.id.opcioAfeguirAE:
                afeguirAE();
                return true;

            case R.id.opcioEliminarAE:
                eliminarAE();
                return true;
            default:
                return super.onContextItemSelected(item);

        }
    }
    ///////////////////////////////////////////////////////////////////////// METODES SOBREESCRITS

    public void cambiarNomAlumne(String n){
        Intent i=new Intent(this,CambiarNomAlumne.class);
        i.putExtra("nomAula",nomAula);
        i.putExtra("nomAlumne",n);
        startActivityForResult(i,CAMBIDENOMALUMNE);

    }


    public void pasarllista(){
        Intent i=new Intent(this,PassarLlista.class);
        i.putExtra("nomAula",nomAula);
        startActivityForResult(i,PASARLLISTA);
    }



    public void eliminarAE(){
         Intent i=new Intent(this,EliminarAE.class);
         i.putExtra("nomAula",nomAula);
         i.putExtra("id",0);         //valor per saber si es tracta de una modificació a nivell de clase o d'alumne
         startActivityForResult(i,ELIMINARAE);

    }


    public void toast(){
        Toast.makeText(this,""+dades.get(obtenirClasse()).alumnes.get(0).notes.get(0).valor,Toast.LENGTH_LONG).show();
    }
}
