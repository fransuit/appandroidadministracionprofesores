package ins.franjupucrack.franju.classroommanagement_v1;



import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.franju.classroommanagement_v1.R;


public class PhotoSelectorDialog extends android.app.DialogFragment {

    private EditText mEditText=null;


    public PhotoSelectorDialog(){

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View dlg= inflater.inflate(R.layout.fragment_select_avatar,container);
        getDialog().setTitle("seleccioneu d'on voleu treure la nova foto");

        Button camera=(Button)dlg.findViewById(R.id.photo);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Student)getActivity()).avatarPhoto();
                dismiss();
            }
        });

        Button galeria=(Button)dlg.findViewById(R.id.gallery);
        galeria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Student)getActivity()).avatarGallery();
                dismiss();
            }
        });

        Button defaultAvatar=(Button)dlg.findViewById(R.id.default_avatar);
        defaultAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Student)getActivity()).defaultAvatar();
                dismiss();
            }
        });

        Button cancel=(Button)dlg.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                  dismiss();
            }
        });

        return dlg;

    }






}
