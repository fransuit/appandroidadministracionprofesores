package ins.franjupucrack.franju.classroommanagement_v1;


import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franju.classroommanagement_v1.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class VisualitzarTarjeta extends Activity {   //totes les tarjetes dun alumne han de tenir asumptes diferents!

    String nomAlumne;
    String nomAula;
    String asumpte;

    ArrayList<DadesAula> dades=new ArrayList<>();

    TextView nomTutor;
    TextView textAsumpte;
    TextView observacions;

    @Override
    public void onCreate(Bundle savedInstasceState) {
        super.onCreate(savedInstasceState);


        setContentView(R.layout.visualitzar_targeta);

        ///////////////////
        obtenirDadesAlumne();
        ////////////////////


        asumpte=getIntent().getExtras().getString("asumpte");   //totes les tarjetes dun alumne han de tenir asumptes diferents!

        nomAlumne=getIntent().getExtras().getString("nomAlumne");
        nomAula=getIntent().getExtras().getString("nomAula");


        nomTutor=findViewById(R.id.textViewNomTutor);
        textAsumpte=findViewById(R.id.textViewAssumpte);
        observacions=findViewById(R.id.textViewObservacions);



       nomTutor.append(dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).targetes.get(obtenirTarjeta()).tutor);

        textAsumpte.append(dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).targetes.get(obtenirTarjeta()).assumpte);

       observacions.append(dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).targetes.get(obtenirTarjeta()).observacio);



    }

    public int obtenirTarjeta(){
        int a=0;
        for(int i=0;i<dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).targetes.size();i++){
            if(dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).targetes.get(i).assumpte.equals(asumpte)){
                a=i;


            }

        }
        return a;

    }



    public void obtenirDadesAlumne(){ //METODE PER RECUPERAR L'ARRAY GUARDADA A LA MEMORIA DEL TELEFON

        DadesAlumne al=null;
        InputStreamReader in;

        try{
            in=new InputStreamReader(openFileInput("dades"));
            BufferedReader bufferedReader=new BufferedReader(in);
            StringBuffer info= new StringBuffer();
            String tmp=null;
            while((tmp=bufferedReader.readLine())!=null){
                info.append(tmp);
            }
            in.close();
            //  SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
            Gson gson=new Gson();
            //String objectes = prefs.getString("b","");
            //String json = new Gson().toJson(objectes);
            JSONArray jsonArray = new JSONArray(info.toString()); //MIrar si es pot sense aquesta linia
            Type listType = new TypeToken<ArrayList<DadesAula>>(){}.getType(); //crec que la versio importada de Type no és la correcta
            dades=new Gson().fromJson(jsonArray.toString(),listType); //json






        }
        catch (Exception e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }



    }


    public int obtenirClasse(){
        int g=0;
        for(int b=0;b<dades.size();b++){
            if(dades.get(b).name.equals(nomAula)){
                g=b;

            }
        }
        return g;


    }
    public int obtenirAlumne(int g){
        int t=0;
        for(int k=0;k<dades.get(g).alumnes.size();k++){
            if(dades.get(g).alumnes.get(k).nom.equals(nomAlumne)){
                t=k;   //MIRAR

            }
        }

        return t;
    }

    public void toast(String a){

        Toast.makeText(this,a,Toast.LENGTH_LONG).show();
    }

}
