package ins.franjupucrack.franju.classroommanagement_v1;


import java.io.Serializable;
import java.util.ArrayList;

public class DadesAlumne implements Serializable {  //clase utilitzada per administrar les dades dels alumnes



    //mes tard afeguir altres dades com per exemple la uri de la imatge de l'alumne, el telefon, el correu,les targetes, un altre array amb apunts sobre lalumne...

    String anotacions="";
    String correu="";

    String correuPares="";


    int asistencies=0;
    int faltes=0;
    int retards=0;

    ArrayList<Targetes> targetes=new ArrayList<Targetes>();  //les targetes que pot acumular un alumne

    //Image posar imatge per cada alumne

    float notaFinal;
    String nom="Alumne CognomAlumne";

    ArrayList<NotesAlumne> notes=new ArrayList<NotesAlumne>();


  public DadesAlumne(String nom){

      this.nom=nom;

  }


    float calcularNotaFinal(){ //  AQUEST METODE CREC QUE SAURA DE POSAR A LA CLASSE CLASSROOM, JA QUE AQUI NO ES FUNCIONAL
        notaFinal=0;
        for(int i=0;i<notes.size();i++){
            float a=notes.get(i).valor;
            float b=notes.get(i).percentatge/100;
            notaFinal+=a*b;


        }
        return notaFinal;
    }



}
