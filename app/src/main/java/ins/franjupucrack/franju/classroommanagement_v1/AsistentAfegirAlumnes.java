package ins.franjupucrack.franju.classroommanagement_v1;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franju.classroommanagement_v1.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class AsistentAfegirAlumnes extends Activity {

    ArrayList<DadesAula> dades=new ArrayList<>();

    String nomAula="";
    ArrayList<DadesAlumne> alumnes=new ArrayList<>();
    EditText entradaNomAlumne;
    Button botoAfeguirAlumneAula;
    Button botoPerCrearAula;
    @Override
    public void onCreate(Bundle savedInstasceState){
        super.onCreate(savedInstasceState);


        setContentView(R.layout.asistent_per_afegir_alumnes);

        //////////////////////
        recuperarDades();
        //////////////////////

        nomAula=getIntent().getExtras().getString("nomAula");

        botoAfeguirAlumneAula=(Button)findViewById(R.id.botoAfegirAlumneAula);
        botoPerCrearAula=(Button)findViewById(R.id.botoPerCrearAulaNova);
        entradaNomAlumne=(EditText)findViewById(R.id.entradaTextNomDeLAlumne);


      if(getIntent().getExtras().getInt("id")==0){       //SI ESTAS CREANT LAULA

          botoAfeguirAlumneAula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(nomAlumnesCorrecte(entradaNomAlumne.getText().toString())) {
                    afeguirAlumne();
                    toast("alumne afeguit");
                    entradaNomAlumne.setText("");
                }else{
                    toast("Aquest nom ja està en ús, escull un altre!");

                }

            }
        });

        botoPerCrearAula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                crearAula();

            }
        });


      }else{                //SI ESTAS AFEGUIT UN ALUMNE A LAULA JA CREADA

          botoPerCrearAula.setText("Guardar");
          botoAfeguirAlumneAula.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                 if(nomCorrecte(entradaNomAlumne.getText().toString())) {
                     afeguirAlumne();
                     toast("alumne afeguit");
                     entradaNomAlumne.setText("");
                 }else{
                     toast("Aquest nom ja està en ús, escull un altre!");

                 }
              }
          });

          botoPerCrearAula.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {

                  guardarAlumnesAfeguits();

              }
          });


      }
    }
    /////////////////////////////////////////////////////
    public int obtenirClasse(){
        int g=0;
        for(int b=0;b<dades.size();b++){
            if(dades.get(b).name.equals(nomAula)){
                g=b;

            }
        }
        return g;


    }


    public boolean nomCorrecte(String s){
        boolean t=true;
        for(int i=0;i<dades.get(obtenirClasse()).alumnes.size();i++){

            if(dades.get(obtenirClasse()).alumnes.get(i).nom.equals(s)){

                t=false;
            }


        }

        return t;
    }


    public boolean nomAlumnesCorrecte(String s){
        boolean t=true;

        for(int i=0;i<alumnes.size();i++){
            if(alumnes.get(i).nom.equals(s)){
                t=false;
            }

        }


        return t;



    }

    public void guardarAlumnesAfeguits(){
        Intent returnIntent=new Intent(); //MIRAR

        returnIntent.putExtra("alumnes",alumnes);

        setResult(RESULT_OK,returnIntent);
        finish(); //ha de retornar a l'activitat anterior

    }

    public void recuperarDades(){ //METODE PER RECUPERAR L'ARRAY GUARDADA A LA MEMORIA DEL TELEFON


        InputStreamReader in;

        try{
            in=new InputStreamReader(openFileInput("dades"));
            BufferedReader bufferedReader=new BufferedReader(in);
            StringBuffer info= new StringBuffer();
            String tmp=null;
            while((tmp=bufferedReader.readLine())!=null){
                info.append(tmp);
            }
            in.close();
            //  SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
            Gson gson=new Gson();
            //String objectes = prefs.getString("b","");
            //String json = new Gson().toJson(objectes);
            JSONArray jsonArray = new JSONArray(info.toString()); //MIrar si es pot sense aquesta linia
            Type listType = new TypeToken<ArrayList<DadesAula>>(){}.getType(); //crec que la versio importada de Type no és la correcta
            dades=new Gson().fromJson(jsonArray.toString(),listType); //json



            TextView tv=(TextView)findViewById(R.id.textQueMostraLlistaBuida);


        }catch (Exception e){
            //Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }

    }
    public void afeguirAlumne(){

        DadesAlumne alumne=new DadesAlumne(entradaNomAlumne.getText().toString());
        alumne.notes=assignarAEAlumne();
        alumnes.add(alumne);

    }

    public void crearAula(){ // UTILITZAR METODE DE MAINACTIVITY JA QUE PER POSAR UNA NOVA AULA ES TE QUE AFEGUIR UN NODO DINS DE LARRAY PRINCIPAL DE LES AULES


        Intent returnIntent=new Intent(); //MIRAR

        returnIntent.putExtra("alumnes",alumnes);               //mirar si encontes de fer aixo es millor detransmetre la informacio directamenta aquesta activity, i dasquesta a la main activity

         setResult(RESULT_OK,returnIntent);
         finish(); //ha de retornar a l'activitat anterior



    }

    public ArrayList<NotesAlumne> assignarAEAlumne(){ //MIRAR

        DadesAlumne al=null;
        InputStreamReader in;
        ArrayList<NotesAlumne> nA=null;
        try{
            in=new InputStreamReader(openFileInput("dades"));
            BufferedReader bufferedReader=new BufferedReader(in);
            StringBuffer info= new StringBuffer();
            String tmp=null;
            while((tmp=bufferedReader.readLine())!=null){
                info.append(tmp);
            }
            in.close();
            //  SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
            Gson gson=new Gson();
            //String objectes = prefs.getString("b","");
            //String json = new Gson().toJson(objectes);
            JSONArray jsonArray = new JSONArray(info.toString()); //MIrar si es pot sense aquesta linia
            Type listType = new TypeToken<ArrayList<DadesAula>>(){}.getType(); //crec que la versio importada de Type no és la correcta
            ArrayList<DadesAula> a=new Gson().fromJson(jsonArray.toString(),listType); //json

            //MIRAR
             int g=0;
            for(int b=0;b<a.size();b++){
                if(a.get(b).name.equals(nomAula)){

                    nA= a.get(b).notes;
                }
            }



        }
        catch (Exception e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }


        return nA;
    }

    public void toast(String s){
        Toast.makeText(this,s,Toast.LENGTH_LONG).show();
    }

}
