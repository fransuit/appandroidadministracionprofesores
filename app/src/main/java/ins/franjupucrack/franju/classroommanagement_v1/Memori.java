package ins.franjupucrack.franju.classroommanagement_v1;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franju.classroommanagement_v1.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class Memori extends Activity {

    ArrayList<DadesAula> dades=new ArrayList<>();
    String nomAula;

    int contador=0;
    TextView tv;
    TextView record;
    @Override
    public void onCreate(Bundle savedInstasceState) {
        super.onCreate(savedInstasceState);

        setContentView(R.layout.memori);

        ////////////////////
        recuperarDades();
        ///////////////////

        nomAula=getIntent().getExtras().getString("nomAula");

        tv=findViewById(R.id.tvContadorFallosMemori);

        record=findViewById(R.id.tvRecordMemori);


        new AlertDialog.Builder(this).setTitle("Poseu el dispositiu en Horizontal").setPositiveButton("ok",new AlertDialog.OnClickListener(){

            public void onClick(DialogInterface dg, int i){

            }
        }).show();


        if(dades.get(obtenirClasse()).minimFallosMemori!=-1){

            record.setText("record(el minim de fallos): "+dades.get(obtenirClasse()).minimFallosMemori);

        }else{

            record.setText("record(el minim de fallos): null");
        }
        //////////////////////////////////////////////////////////////////////////////////////////// Algoritme quantitatiu
        //obtenir numero de files i de columnes posibles segons la quantitat dalumnes





        int rangoX;
        int rangoY;
        int elsKFalten=0;

        if((dades.get(obtenirClasse()).alumnes.size()*2)<=6){
            //Toast.makeText(this,""+dades.get(obtenirClasse()).alumnes.size()*2,Toast.LENGTH_LONG).show();
            rangoX=dades.get(obtenirClasse()).alumnes.size()*2;
            rangoY=1;
        }else{
            //Toast.makeText(this,""+dades.get(obtenirClasse()).alumnes.size() * 2,Toast.LENGTH_LONG).show();
            rangoX=6;

            /////////////////////////////////doubles per poder realitzar loperacio correctament
            double t=dades.get(obtenirClasse()).alumnes.size();
            double dos=2;
            double sis=6;
            double poc=0.1;
            //////////////////////////////////

            double p=(t * dos) / sis;

            int p2=(dades.get(obtenirClasse()).alumnes.size() * 2) / 6;
            if(p!=p2) {
                //mirar si es poden comparar float i int
                rangoY = ((dades.get(obtenirClasse()).alumnes.size() * 2) / 6) + 1;
                double er=rangoY;
                double def=(er-p)*sis;


                elsKFalten=(int)def;

                double r=def+poc;
                int r2=(int)r;

                if(r2==elsKFalten+1){
                  elsKFalten++;

                }


               // Toast.makeText(this,""+elsKFalten+"/ "+def,Toast.LENGTH_LONG).show();
            }else{

                rangoY = (dades.get(obtenirClasse()).alumnes.size() * 2) / 6;
            }

        }


        ///////////////////////////////////////////////////////////////////////////////////






        DisplayMetrics dm=new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int widthTv=dm.widthPixels / 6;
        int heightTv=dm.heightPixels / 8;


        LinearLayout linearLayout=findViewById(R.id.tableroMemori);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        int q=0;
        for(int i=0;i<rangoY;i++) {

            LinearLayout l = new LinearLayout(this);
            l.setOrientation(android.widget.LinearLayout.HORIZONTAL);
            if (i == rangoY - 1) {
                for (int y = 0; y < rangoX-elsKFalten; y++) {

                    //mirar que el numero que sintrodueixi sigui lultimde la llista del bucle anterior
                    ButtonMemori b = new ButtonMemori(this, "nomPerDefecte");
                    b.setWidth(widthTv);
                    b.setHeight(heightTv);
                    b.setId(q + 77777);

                    q++;


                    l.addView(b);

                }


                linearLayout.addView(l);
            }else{
                for (int y = 0; y < rangoX; y++) {


                    //mirar que el numero que sintrodueixi sigui lultimde la llista del bucle anterior
                    ButtonMemori b = new ButtonMemori(this, "nomPerDefecte");
                    b.setWidth(widthTv);
                    b.setHeight(heightTv);
                    b.setId(q + 77777);


                    q++;


                    l.addView(b);

                }


                linearLayout.addView(l);

            }

        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        asignarInfoBotons();

        for (int i = 0; i < dades.get(obtenirClasse()).alumnes.size()*2; i++) {

            assignarAccioBoto(i+77777);
        }


    }


    public void asignarInfoBotons() {

        for (int i = 0; i < dades.get(obtenirClasse()).alumnes.size()*2; i++) {

            ButtonMemori b = findViewById(i+77777);

            boolean incorrecte=true;
            while(incorrecte) {

                String j=dades.get(obtenirClasse()).alumnes.get((int) (Math.random() * dades.get(obtenirClasse()).alumnes.size())).nom;
                if (esPotPosar(j)) {
                     b.nomAlumne = j;
                    incorrecte=false;
                }
            }

        }
    }

    public void assignarAccioBoto(int id){
        final ButtonMemori b = findViewById(id);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //si ja s'ha destapat la segona peça
               if(hiHaAlgunClicat()) {

                   if(!b.trobat){
                   b.setText(b.nomAlumne);

                   if (consideixen(b.nomAlumne)) { //si es compleix vol dir que s'ha encertat

                       if (consideixenID(b.nomAlumne) != 0) {
                           ButtonMemori b1 = findViewById(consideixenID(b.nomAlumne));
                           b1.setText(b1.nomAlumne);


                           //b1.setVisibility(View.GONE);
                           //b.setVisibility(View.GONE);
                           b1.trobat = true;
                           b.trobat = true;
                           b1.clicat = false;
                           b.clicat = false;
                           if (hasGuanyat()) {
                               //POSAR UN DIALOG O ALGO AIXI,I ACABAR

                               if(contador<dades.get(obtenirClasse()).minimFallosMemori || dades.get(obtenirClasse()).minimFallosMemori==-1){
                                   dades.get(obtenirClasse()).minimFallosMemori=contador;
                                   guardarDades();

                               }

                               toast();
                           }


                       }


                   } else {
                       contador++;
                       ButtonMemori b2 = findViewById(trobarElementClicat());
                       b2.setText(b2.nomAlumne);
                       //AQUI HA HA DAVER UNA PAUSA D'UN SEGON
                       b.setText("");
                       b2.setText("");
                       tv.setText("fallos: "+contador);
                       b2.clicat = false;
                       b.clicat = false;

                   }
               }

               }


               //si s'ha destapat la primera peça
               else{

                   if(!b.trobat) {
                       b.setText(b.nomAlumne);
                       b.clicat = true;
                   }
               }
            }
        });

    }

    public void toast(){Toast.makeText(this,"has guanyat!!!",Toast.LENGTH_LONG).show();}

    public int trobarElementClicat(){
        int u=0;
        for (int i = 0; i < dades.get(obtenirClasse()).alumnes.size()*2; i++) {
            ButtonMemori b = findViewById(i+77777);
            if(b.clicat){
                u=b.getId();
            }

        }
        return u;

    }

    public boolean hasGuanyat(){
        int a=0;
        boolean victoria=false;
        for (int i = 0; i < dades.get(obtenirClasse()).alumnes.size()*2; i++) {
            ButtonMemori b = findViewById(i+77777);
                if(b.trobat){
                    a++;
                }

        }
        if(a==dades.get(obtenirClasse()).alumnes.size()*2){
            victoria=true;

        }

      return victoria;
    }

    public int consideixenID(String n){
        int r=0;
        for (int i = 0; i < dades.get(obtenirClasse()).alumnes.size()*2; i++) {
            ButtonMemori b = findViewById(i+77777);
            if(b.clicat){
                if(b.nomAlumne.equals(n)){
                    r=b.getId();
                }

            }
        }
        return r;
    }


    public boolean consideixen(String n){
        boolean r=false;
        for (int i = 0; i < dades.get(obtenirClasse()).alumnes.size()*2; i++) {
            ButtonMemori b = findViewById(i+77777);
            if(b.clicat){
                if(b.nomAlumne.equals(n)){
                    r=true;
                }

            }
        }
        return r;
    }

    public boolean hiHaAlgunClicat(){
        boolean r=false;
        for (int i = 0; i < dades.get(obtenirClasse()).alumnes.size()*2; i++) {
            ButtonMemori b = findViewById(i+77777);
            if(b.clicat){
                r=true;
            }

        }

        return r;
    }


    public boolean esPotPosar(String n){
        boolean r=true;
        int a=0;

        for (int i = 0; i < dades.get(obtenirClasse()).alumnes.size()*2; i++) {
            ButtonMemori b = findViewById(i+77777);
               if(b.nomAlumne.equals(n)){
                   a++;
               }
        }
        if(a==2 || a>2){
            r=false;
        }

        return r;
     }
    public void guardarDades(){  //METODE PER GUARDAR L'ARRAY A LA MEMORIA DEL TELÈFON

        OutputStreamWriter out;

        try{
            // SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);

            out=new OutputStreamWriter(openFileOutput("dades",0));


            String jsonObjetos = new Gson().toJson(dades);
            //SharedPreferences.Editor editor=prefs.edit();
            // editor.putString("b",jsonObjetos);//jsonObjetos

            out.write(jsonObjetos);
            out.flush();
            out.close();
            Toast.makeText(this,"Guardant...",Toast.LENGTH_SHORT).show();
            //editor.clear(); ///UTILITZAR?
            //editor.apply();

        }catch (Exception e){
            //Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }

    }

    public void recuperarDades(){ //METODE PER RECUPERAR L'ARRAY GUARDADA A LA MEMORIA DEL TELEFON


        InputStreamReader in;

        try{
            in=new InputStreamReader(openFileInput("dades"));
            BufferedReader bufferedReader=new BufferedReader(in);
            StringBuffer info= new StringBuffer();
            String tmp=null;
            while((tmp=bufferedReader.readLine())!=null){
                info.append(tmp);
            }
            in.close();
            //  SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
            Gson gson=new Gson();
            //String objectes = prefs.getString("b","");
            //String json = new Gson().toJson(objectes);
            JSONArray jsonArray = new JSONArray(info.toString()); //MIrar si es pot sense aquesta linia
            Type listType = new TypeToken<ArrayList<DadesAula>>(){}.getType(); //crec que la versio importada de Type no és la correcta
            dades=new Gson().fromJson(jsonArray.toString(),listType); //json



            TextView tv=(TextView)findViewById(R.id.textQueMostraLlistaBuida);


        }catch (Exception e){
            //Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }

    }
    public int obtenirClasse(){
        int g=0;
        for(int b=0;b<dades.size();b++){
            if(dades.get(b).name.equals(nomAula)){
                g=b;

            }
        }
        return g;


    }

}
