package ins.franjupucrack.franju.classroommanagement_v1;


import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.franju.classroommanagement_v1.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class CrearNovaTargeta extends Activity {


    String nomAula;
    String nomAlumne;
    EditText nomTutor;
    EditText asumpte;
    EditText observacio;

    ArrayList<DadesAula> dades=new ArrayList<>();
    @Override
    public void onCreate(Bundle savedInstasceState) {
        super.onCreate(savedInstasceState);

          setContentView(R.layout.crear_nova_targeta);

          /////////////////////
          obtenirDadesAlumne();
          /////////////////////


          nomAlumne=getIntent().getExtras().getString("nomAlumne");
          nomAula=getIntent().getExtras().getString("nomAula");
          nomTutor=findViewById(R.id.editTextNomTutor);
          asumpte=findViewById(R.id.editTextAssumpteTargeta);
          observacio=findViewById(R.id.editTextDescripcioFets);


        Button b=findViewById(R.id.botoPerCrearTargeta);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              if(nomCorrecte(asumpte.getText().toString())) {
                  crearTarjeta();

              } else{
                  toast("Canvia el assumpte, perquè aquest ja esta en ús!");

              }
            }
        });



    }

    public void toast(String s){
        Toast.makeText(this,s,Toast.LENGTH_LONG).show();
    }

    public void crearTarjeta(){
        Targetes t=new Targetes(asumpte.getText().toString(),observacio.getText().toString(),nomAlumne,nomAula,nomTutor.getText().toString());

        dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).targetes.add(t);
        //Intent i=new Intent();
        //i.putExtra("tarjetaCreada",t);
        guardarDades();
       // setResult(RESULT_OK,i);




        setResult(RESULT_OK);
        finish();

    }
    public boolean nomCorrecte(String s){
        boolean t=true;
        for(int i=0;i<dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).targetes.size();i++){

            if(dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).targetes.get(i).assumpte.equals(s)){

                t=false;
            }


        }

        return t;
    }



    public void guardarDades(){

        OutputStreamWriter out;

        try{


            //dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes = a.notes;
            out=new OutputStreamWriter(openFileOutput("dades",0));


            String jsonObjetos = new Gson().toJson(dades);


            out.write(jsonObjetos);
            out.flush();
            out.close();
            Toast.makeText(this,"Guardant...",Toast.LENGTH_SHORT).show();


        }catch (Exception e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }

    }


    public void obtenirDadesAlumne(){ //METODE PER RECUPERAR L'ARRAY GUARDADA A LA MEMORIA DEL TELEFON

        DadesAlumne al=null;
        InputStreamReader in;

        try{
            in=new InputStreamReader(openFileInput("dades"));
            BufferedReader bufferedReader=new BufferedReader(in);
            StringBuffer info= new StringBuffer();
            String tmp=null;
            while((tmp=bufferedReader.readLine())!=null){
                info.append(tmp);
            }
            in.close();
            //  SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
            Gson gson=new Gson();
            //String objectes = prefs.getString("b","");
            //String json = new Gson().toJson(objectes);
            JSONArray jsonArray = new JSONArray(info.toString()); //MIrar si es pot sense aquesta linia
            Type listType = new TypeToken<ArrayList<DadesAula>>(){}.getType(); //crec que la versio importada de Type no és la correcta
            dades=new Gson().fromJson(jsonArray.toString(),listType); //json




            int g=0;
            for(int b=0;b<dades.size();b++){
                if(dades.get(b).name.equals(nomAula)){
                    g=b;
                    //notesAlumne= a.get(b).notes;   //NO FA FALTA
                }
            }
            for(int k=0;k<dades.get(g).alumnes.size();k++){
                if(dades.get(g).alumnes.get(k).nom.equals(nomAlumne)){
                    //al=a.get(g).alumnes.get(k);  //MIRAR

                }
            }


        }
        catch (Exception e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }



    }

    public int obtenirClasse(){
        int g=0;
        for(int b=0;b<dades.size();b++){
            if(dades.get(b).name.equals(nomAula)){
                g=b;

            }
        }
        return g;


    }
    public int obtenirAlumne(int g){
        int t=0;
        for(int k=0;k<dades.get(g).alumnes.size();k++){
            if(dades.get(g).alumnes.get(k).nom.equals(nomAlumne)){
                t=k;   //MIRAR

            }
        }

        return t;
    }



}
