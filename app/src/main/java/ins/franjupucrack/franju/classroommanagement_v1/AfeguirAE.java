package ins.franjupucrack.franju.classroommanagement_v1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franju.classroommanagement_v1.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;


public class AfeguirAE extends Activity {

    ArrayList<NotesAlumne> notes=new ArrayList<>();  //es crea una arraylist de notes que despres s'afeguiran a l'arrailist de notes de la classe determinada

    ArrayList<DadesAula> aules =new ArrayList<>();

    String nomAula;
    EditText entradaNomNota;
    EditText entradaPercentatge;
    Button botoAfeguirAE;
    Button botoGuardarAENoves;
    TextView tvAE;
    String nomAlumne;
    @Override
    public void onCreate(Bundle savedInstasceState) {
        super.onCreate(savedInstasceState);

        setContentView(R.layout.afeguir_ae);

        ////////////////////////
        recuperarDades();
        ////////////////////////

        entradaNomNota=findViewById(R.id.entradaTextNomNovaActivitatEvaluacio);
        entradaPercentatge=findViewById(R.id.entradaTextPercentatgeNovaActivitatEvaluacio);
        botoAfeguirAE=findViewById(R.id.botoPerAENova);
        botoGuardarAENoves=findViewById(R.id.botoPerGuadarAENoves);
        tvAE=findViewById(R.id.tvtextAE);
        if(getIntent().getExtras().getInt("id")==1){
            tvAE.setText("Afegueix les activitats d'evaluació que desitgiper l'alumne:"+getIntent().getExtras().getString("nomAlumne"));
            nomAlumne=getIntent().getExtras().getString("nomAlumne");
            nomAula=getIntent().getExtras().getString("nomAula");
        }else{
            nomAula=getIntent().getExtras().getString("nomAula");

        }

        botoAfeguirAE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             if(getIntent().getExtras().getInt("id")==1) {
                 if (nomCorrecteAEAlumne(entradaNomNota.getText().toString()) && nomNotaCorrecte(entradaNomNota.getText().toString())) {
                     try {
                         afeguirNotes();
                         toast("Activitat d'evaluació afeguida");
                         entradaNomNota.setText("");
                         entradaPercentatge.setText("");
                     }catch (Exception e){
                         toast("valor incorrecte");

                     }
                 } else {

                     toast("Aquest nom ja està en us, escull un altre!");
                 }

             }else{
                 if (nomNotaCorrecte(entradaNomNota.getText().toString()) && nomCorrecte(entradaNomNota.getText().toString())) {

                     try {
                     afeguirNotes();
                     toast("Activitat d'evaluació afeguida");
                     entradaNomNota.setText("");
                     entradaPercentatge.setText("");

                     }catch (Exception e){
                         toast("valor incorrecte");

                     }
                 } else {

                     toast("Aquest nom ja està en us, escull un altre!");
                 }


             }
            }
        });

        botoGuardarAENoves.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                guardar();

            }
        });


    }
    public int obtenirAlumne(int g){
        int t=0;
        for(int k=0;k<aules.get(g).alumnes.size();k++){
            if(aules.get(g).alumnes.get(k).nom.equals(nomAlumne)){
                t=k;   //MIRAR

            }
        }

        return t;
    }


    public int obtenirClasse(){
        int g=0;
        for(int b=0;b<aules.size();b++){
            if(aules.get(b).name.equals(nomAula)){
                g=b;

            }
        }
        return g;


    }

    public boolean nomCorrecteAEAlumne(String s){
        boolean t=true;
        for(int i=0;i<aules.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes.size();i++){

            if(aules.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes.get(i).nom.equals(s)){

                t=false;
            }


        }

        return t;
    }


    public boolean nomCorrecte(String s){
        boolean t=true;
        for(int i=0;i<aules.get(obtenirClasse()).notes.size();i++){

            if(aules.get(obtenirClasse()).notes.get(i).nom.equals(s)){

                t=false;
            }


        }

        return t;
    }
    public boolean nomNotaCorrecte(String s){
        boolean t=true;

        for(int i=0;i<notes.size();i++){
            if(notes.get(i).nom.equals(s)){
                t=false;
            }

        }


        return t;



    }
    public void afeguirNotes(){
        NotesAlumne n=new NotesAlumne(entradaNomNota.getText().toString(),Float.parseFloat(entradaPercentatge.getText().toString()));
        notes.add(n);
    }

    public void guardar(){
        Intent returnIntent=new Intent(); //MIRAR

        returnIntent.putExtra("notes",notes);

        setResult(RESULT_OK,returnIntent);
        finish(); //ha de retornar a l'activitat anterior

    }
    public void recuperarDades(){ //METODE PER RECUPERAR L'ARRAY GUARDADA A LA MEMORIA DEL TELEFON


        InputStreamReader in;

        try{
            in=new InputStreamReader(openFileInput("dades"));
            BufferedReader bufferedReader=new BufferedReader(in);
            StringBuffer info= new StringBuffer();
            String tmp=null;
            while((tmp=bufferedReader.readLine())!=null){
                info.append(tmp);
            }
            in.close();
            //  SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
            Gson gson=new Gson();
            //String objectes = prefs.getString("b","");
            //String json = new Gson().toJson(objectes);
            JSONArray jsonArray = new JSONArray(info.toString());
            Type listType = new TypeToken<ArrayList<DadesAula>>(){}.getType();
            aules=new Gson().fromJson(jsonArray.toString(),listType); //json



            TextView tv=(TextView)findViewById(R.id.textQueMostraLlistaBuida);


        }catch (Exception e){
            //Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }

    }


    public void toast(String s){
        Toast.makeText(this,s,Toast.LENGTH_LONG).show();
    }
}
