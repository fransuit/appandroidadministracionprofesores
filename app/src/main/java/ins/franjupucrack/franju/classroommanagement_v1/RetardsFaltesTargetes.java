package ins.franjupucrack.franju.classroommanagement_v1;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franju.classroommanagement_v1.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class RetardsFaltesTargetes extends Activity {     //totes les tarjetes dun alumne han de tenir asumptes diferents!

    ArrayList<DadesAula> dades=new ArrayList<>();

    EditText faltes;
    EditText retards;
    ListView targetes;

    String nomAlumne;
    String nomAula;

    final int NOVATARJETA=191919;

    @Override
    public void onCreate(Bundle savedInstasceState) {
        super.onCreate(savedInstasceState);

        setContentView(R.layout.faltes_retards_targetes);


        nomAlumne=getIntent().getExtras().getString("nomAlumne");
        nomAula=getIntent().getExtras().getString("nomAula");


        ////////////////////
        obtenirDadesAlumne();
        ////////////////////

        targetes=findViewById(R.id.listViewTargetes);
        retards=findViewById(R.id.editTextNumRetards);
        faltes=findViewById(R.id.editTextNumFaltes);

        retards.setText(""+dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).retards);
        faltes.setText(""+dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).faltes);

        ////////////////////
        posarDadesALaLlista();
        ////////////////////

        targetes=findViewById(R.id.listViewTargetes);
        registerForContextMenu(targetes);
        targetes.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){

                ValorsLlista v=new ValorsLlista();
                v=(ValorsLlista) parent.getItemAtPosition(position);
                //ValorsLlista v2=(ValorsLlista) view.get;
                visualitzarTarjeta(v.asumpte);
                //toast(v.asumpte);

            }

        });

        FloatingActionButton fb=findViewById(R.id.botoOpcionsTargetesAlumne);
        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerForContextMenu(view);
                openContextMenu(view);

            }
        });


        Button guardar=findViewById(R.id.botoGuardarTargetesDunAlumne);
        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                guardarValors();

                }catch (Exception e){
                    toast("valor incorrecte");

                }
            }
        });





    }

    public void guardarValors(){
        dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).faltes=Integer.parseInt(faltes.getText().toString());
        dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).retards=Integer.parseInt(retards.getText().toString());
        guardarCanvis();


    }


    public void visualitzarTarjeta(String asumpte){
        Intent i=new Intent(this, VisualitzarTarjeta.class);
        i.putExtra("nomAlumne",nomAlumne);
        i.putExtra("nomAula",nomAula);
        i.putExtra("asumpte",asumpte);

        startActivity(i);

    }



    @Override
    public void onActivityResult(int requestCode,int resultCode, Intent i){
        if(requestCode==NOVATARJETA){
            if(resultCode==RESULT_OK){

                //dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).targetes.add((ArrayList<Targetes>)i.getSerializableExtra("tarjetaCreada"));
                obtenirDadesAlumne();
                posarDadesALaLlista();
            }

        }


    }



    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){  //menu per cuan mantens pulsat un element de la llista
        super.onCreateContextMenu(menu,v,menuInfo);
        MenuInflater inflater;
        switch (v.getId()){
            case R.id.listViewTargetes:
                inflater=getMenuInflater();
                inflater.inflate(R.menu.menu_clicar_layout_targeta,menu);

                break;
            case R.id.botoOpcionsTargetesAlumne:
                inflater=getMenuInflater();
                inflater.inflate(R.menu.menu_faltes_retards_targetes,menu);

                break;
        }


    }




    @Override
    public boolean onContextItemSelected(MenuItem item){
        AdapterView.AdapterContextMenuInfo delw=(AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        //mLastRowSelected=delw.position; -> mirar si es necesari


        switch (item.getItemId()){
            case R.id.borrarTargeta:
                final String n=dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).targetes.get(delw.position).assumpte; //MIRAR , OBTENIR EL NOM DE LAULA PER SABER DE QUINA ES TRACTA, MIRAR SI ES MILLOR UTILITZAR UN ID
                new AlertDialog.Builder(this).setTitle("segur que vols eliminar la tarjeta?").setPositiveButton("si",new AlertDialog.OnClickListener(){

                    public void onClick(DialogInterface dg, int i){
                        //toast();
                        eliminarTarjeta(n);
                    }
                }).setNegativeButton("no",null).show();
                return true;

            case R.id.novaTargeta:
                novaTarjeta();
                return true;
            default:
                return super.onContextItemSelected(item);

        }
    }



    public void eliminarTarjeta(String n){
        int k=0;



        for(int i=0;i<dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).targetes.size();i++){


            if(dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).targetes.get(i).assumpte.equals(n)){

                dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).targetes.remove(i);
            }
        }
        // Toast.makeText(this,""+dades.get(k).alumnes.get(0).notes.get(0).valor,Toast.LENGTH_LONG).show();
        guardarCanvis(); //potser s'auria de treure aixo
        posarDadesALaLlista();

    }

    public void novaTarjeta(){
        Intent i=new Intent(this,CrearNovaTargeta.class);
        i.putExtra("nomAula",nomAula);
        i.putExtra("nomAlumne",nomAlumne);
        startActivityForResult(i,NOVATARJETA);

    }
    public void obtenirDadesAlumne(){ //METODE PER RECUPERAR L'ARRAY GUARDADA A LA MEMORIA DEL TELEFON

        DadesAlumne al=null;
        InputStreamReader in;

        try{
            in=new InputStreamReader(openFileInput("dades"));
            BufferedReader bufferedReader=new BufferedReader(in);
            StringBuffer info= new StringBuffer();
            String tmp=null;
            while((tmp=bufferedReader.readLine())!=null){
                info.append(tmp);
            }
            in.close();
            //  SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
            Gson gson=new Gson();
            //String objectes = prefs.getString("b","");
            //String json = new Gson().toJson(objectes);
            JSONArray jsonArray = new JSONArray(info.toString()); //MIrar si es pot sense aquesta linia
            Type listType = new TypeToken<ArrayList<DadesAula>>(){}.getType(); //crec que la versio importada de Type no és la correcta
            dades=new Gson().fromJson(jsonArray.toString(),listType); //json




            int g=0;
            for(int b=0;b<dades.size();b++){
                if(dades.get(b).name.equals(nomAula)){
                    g=b;
                    //notesAlumne= a.get(b).notes;   //NO FA FALTA
                }
            }
            for(int k=0;k<dades.get(g).alumnes.size();k++){
                if(dades.get(g).alumnes.get(k).nom.equals(nomAlumne)){
                    //al=a.get(g).alumnes.get(k);  //MIRAR

                }
            }


        }
        catch (Exception e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }



    }

    public void posarDadesALaLlista() {   //metode per fer que la llista de les classes


        ArrayList<ValorsLlista> resultList = new ArrayList<ValorsLlista>();

        for(int i=0;i<dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).targetes.size();i++) {                  //AQUEST BUCLE OMPLE LES DADES DE LARRAY RESULTLIST

            ValorsLlista vl = new ValorsLlista();
            vl.asumpte = dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).targetes.get(i).assumpte;               //afegir més coses si es vol afeguir coses als layouts de la llista
            resultList.add(vl);

        }

        Adaptadorllista adaptador = new Adaptadorllista(this, R.layout.layout_per_list_adapter_targetes_alumne, resultList, getLayoutInflater());

        ListView listView = (ListView) findViewById(R.id.listViewTargetes);
        listView.setAdapter(adaptador);                 //ADAPTADOR DE LA LLISTA

        /*
        if (dades.get(obtenirClasse()).alumnes.isEmpty()) {

            TextView tv = (TextView) findViewById(R.id.textViewPerDirQueNoHiHaAlumnes);
            tv.setText("NO hi han alumnes assignats");

        } else {
            TextView tv = (TextView) findViewById(R.id.textViewPerDirQueNoHiHaAlumnes);
            tv.setText("");                                                             //MIRAR SI ES MILLOR POSAR UN HIDE
        }
        */



    }


    public void guardarCanvis(){

        OutputStreamWriter out;

        try{


            out=new OutputStreamWriter(openFileOutput("dades",0));


            String jsonObjetos = new Gson().toJson(dades);

            out.write(jsonObjetos);
            out.flush();
            out.close();
            Toast.makeText(this,"Guardant...",Toast.LENGTH_SHORT).show();


        }catch (Exception e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }
    }


    public static class ValorsLlista{
        String asumpte="";

    }



    public int obtenirClasse(){
        int g=0;
        for(int b=0;b<dades.size();b++){
            if(dades.get(b).name.equals(nomAula)){
                g=b;

            }
        }
        return g;


    }
    public int obtenirAlumne(int g){
        int t=0;
        for(int k=0;k<dades.get(g).alumnes.size();k++){
            if(dades.get(g).alumnes.get(k).nom.equals(nomAlumne)){
                t=k;   //MIRAR

            }
        }

        return t;
    }





    public static class Adaptadorllista extends ArrayAdapter {               //classe que hereda de arrayAdapter i que permet que la listview mostri el contingut d'una array
        private LayoutInflater mInflater;
        private List<ValorsLlista> mObjects;

        public Adaptadorllista(Context context, int resource, List<ValorsLlista> objects, LayoutInflater inflater){                    //FER AIXOO
            super(context,resource,objects);
            mInflater=inflater;
            mObjects=objects;

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            ValorsLlista valorsLlista=mObjects.get(position);
            View row=mInflater.inflate(R.layout.layout_per_list_adapter_targetes_alumne,null,false);

            TextView tv=(TextView) row.findViewById(R.id.textPerLayoutDeArrayAdapterTargetes);
            tv.setText(valorsLlista.asumpte); //se li assigna el nom de l'aula al text view que es mostrara a la llista d'aules

            return row;

        }


    }

    public void toast(String a){

        Toast.makeText(this,a,Toast.LENGTH_LONG).show();
    }

}
