package ins.franjupucrack.franju.classroommanagement_v1;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franju.classroommanagement_v1.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;


public class EliminarAE extends Activity {
    ;
    ArrayList<DadesAula> dades=new ArrayList<>();
    String nomAula;
    String nomAlumne;//nomes sutilitza cuan es vol eliminar ae dun alumne
    ArrayList<NotesAlumne>notes=new ArrayList<>(); //nose si fa falta



    @Override
    public void onCreate(Bundle savedInstasceState) {
        super.onCreate(savedInstasceState);

        setContentView(R.layout.eliminar_ae);




        nomAula=getIntent().getExtras().getString("nomAula");

        //////////////////
        recuperarDades();
        //////////////////


        DisplayMetrics dm=new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int widthTv=dm.widthPixels / 4;
        int heightTv=dm.heightPixels / 10;
        int widthb=dm.widthPixels / 3;
        int heightb=dm.heightPixels / 10;


        if(getIntent().getExtras().getInt("id")==0){  //SI ES VOL FER PER TOTA LA CLASSE


            LinearLayout linearLayout=findViewById(R.id.layoutPerEliminarAE);

            for(int k=0;k<dades.get(obtenirClasse()).notes.size();k++){

                TextView tv=new TextView(this);      ///nom AE
                tv.setText(dades.get(obtenirClasse()).notes.get(k).nom);
                tv.setWidth(widthTv);
                tv.setHeight(heightTv);
                tv.setId(k+2000);
                tv.invalidate();

                Button b=new Button(this);
                b.setText("eliminar");                     //AL ELIMINAR UNA ACTIVITAT DEVALUACIO ELS CANVIA SAPLICARAN PER A TOTS ELS ALUMNES DE LA MATEIXA AULA
                b.setWidth(widthb);
                b.setHeight(heightb);
                b.setId(k+1000);
                b.invalidate();


                LinearLayout l=new LinearLayout(this);


                l.setOrientation(LinearLayout.HORIZONTAL);
                l.addView(tv);
                l.addView(b);
                if(l==null){
                    Toast.makeText(this, "es null", Toast.LENGTH_LONG).show();
                }
                linearLayout.addView(l);

            }


            for(int g=0;g<dades.get(obtenirClasse()).notes.size();g++){ //MIRAR
               asignarFuncioBoto(g);

            }

            Button guardar =findViewById(R.id.buttonGuardarAEeliminades);
            guardar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    guardar();
                }
            });








        }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        else{   //SI ES VOL FER PER NOMES UN ALUMNE

            LinearLayout linearLayout=findViewById(R.id.layoutPerEliminarAE);
            nomAlumne=getIntent().getExtras().getString("nomAlumne");

            for(int k=0;k<dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes.size();k++){

                TextView tv=new TextView(this);      ///nom AE
                tv.setText(dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes.get(k).nom);
                tv.setWidth(widthTv);
                tv.setHeight(heightTv);
                tv.setId(k+2000);
                tv.invalidate();

                Button b=new Button(this);
                b.setText("eliminar");                     //AL ELIMINAR UNA ACTIVITAT DEVALUACIO ELS CANVIA SAPLICARAN PER A TOTS ELS ALUMNES DE LA MATEIXA AULA
                b.setWidth(widthb);
                b.setHeight(heightb);
                b.setId(k+1000);
                b.invalidate();


                LinearLayout l=new LinearLayout(this);


                l.setOrientation(LinearLayout.HORIZONTAL);
                l.addView(tv);
                l.addView(b);
                if(l==null){
                    Toast.makeText(this, "es null", Toast.LENGTH_LONG).show();
                }
                linearLayout.addView(l);

            }


            for(int g=0;g<dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes.size();g++){ //MIRAR
                asignarFuncioBotoAlumne(g);

            }

            Button guardar =findViewById(R.id.buttonGuardarAEeliminades);
            guardar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    guardarAlumne();
                }
            });












        }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }

    public void asignarFuncioBoto(int id){
        Button b=findViewById(id+1000);
        final int d=id;
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                notes.add(dades.get(obtenirClasse()).notes.get(d));
                visibilitatBoto(d);
                //dades.get(obtenirClasse()).notes.remove(d);

            }
        });
    }

    public void asignarFuncioBotoAlumne(int id){
        Button b=findViewById(id+1000);
        final int d=id;
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                notes.add(dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes.get(d));
                visibilitatBoto(d);
                //dades.get(obtenirClasse()).notes.remove(d);

            }
        });
    }



    public void guardar() {

        dades.get(obtenirClasse()).notes.removeAll(notes);//dades de la classe

        for(int s=0;s<dades.get(obtenirClasse()).alumnes.size(); s++) {


            ArrayList<NotesAlumne>t=new ArrayList<>();
            for(int g=0;g<dades.get(obtenirClasse()).alumnes.get(s).notes.size();g++){
                int h=0;
                for(int l=0;l<notes.size();l++){

                   if(!dades.get(obtenirClasse()).alumnes.get(s).notes.get(g).nom.equals(notes.get(l).nom)){
                       h++;
                   }
                }
                if(h==notes.size()){
                    t.add(dades.get(obtenirClasse()).alumnes.get(s).notes.get(g));
                }

            }
            //aqui
            dades.get(obtenirClasse()).alumnes.get(s).notes=t;
    }

     guardarCanvis();
        setResult(RESULT_OK);
       finish();
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void guardarAlumne() {



            ArrayList<NotesAlumne>t=new ArrayList<>();
            for(int g=0;g<dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes.size();g++){
                int h=0;
                for(int l=0;l<notes.size();l++){

                    if(!(dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes.get(g).nom).equals(notes.get(l).nom)){
                        h++;
                    }
                }
                if(h==notes.size()){
                    t.add(dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes.get(g));
                }

            }
            //aqui
        dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes=t;


        guardarCanvis();
        setResult(RESULT_OK);
        finish();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////



    public void visibilitatBoto(int id){
        Button b=findViewById(id+1000);
        b.setVisibility(View.GONE);
        TextView tv=findViewById(id+2000);
        tv.setVisibility(View.GONE);
    }



    public void recuperarDades(){ //METODE PER RECUPERAR L'ARRAY GUARDADA A LA MEMORIA DEL TELEFON


        InputStreamReader in;

        try{
            in=new InputStreamReader(openFileInput("dades"));
            BufferedReader bufferedReader=new BufferedReader(in);
            StringBuffer info= new StringBuffer();
            String tmp=null;
            while((tmp=bufferedReader.readLine())!=null){
                info.append(tmp);
            }
            in.close();
            //  SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
            Gson gson=new Gson();
            //String objectes = prefs.getString("b","");
            //String json = new Gson().toJson(objectes);
            JSONArray jsonArray = new JSONArray(info.toString()); //MIrar si es pot sense aquesta linia
            Type listType = new TypeToken<ArrayList<DadesAula>>(){}.getType(); //crec que la versio importada de Type no és la correcta
            dades=new Gson().fromJson(jsonArray.toString(),listType); //json






        }catch (Exception e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }

    }

    public void guardarCanvis(){

        OutputStreamWriter out;

        try{


            out=new OutputStreamWriter(openFileOutput("dades",0));


            String jsonObjetos = new Gson().toJson(dades);

            out.write(jsonObjetos);
            out.flush();
            out.close();
            Toast.makeText(this,"Guardant...",Toast.LENGTH_SHORT).show();


        }catch (Exception e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }
    }

    public int obtenirClasse(){
        int g=0;
        for(int b=0;b<dades.size();b++){
            if(dades.get(b).name.equals(nomAula)){
                g=b;

            }
        }
        return g;


    }




    public int obtenirAlumne(int g){
        int t=0;
        for(int k=0;k<dades.get(g).alumnes.size();k++){
            if(dades.get(g).alumnes.get(k).nom.equals(nomAlumne)){
                t=k;   //MIRAR

            }
        }

        return t;
    }


    public void toast(String s){
        Toast.makeText(this,s,Toast.LENGTH_LONG).show();
    }
}





















