package ins.franjupucrack.franju.classroommanagement_v1;



import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.util.DisplayMetrics;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franju.classroommanagement_v1.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class Student extends Activity {

    String nomAlumne;
    String nomAula;
    DadesAlumne alumne;                //NO SAURIA DUTILITZAR
    ArrayList<NotesAlumne> notesAlumne;  //NO SAURIA DUTILITZAR
    ArrayList<DadesAula> dades;

    ImageButton avatar;

    final int FRT=7774;
    final int CONTACTAR=91928;
    final int APUNTS=55568;
    final int ELIMINARAE=3245;
    final int AFEGUIRAE=3412;

    static final int ACTION_AVATAR_PHOTO=1;
    static final int ACTION_AVATAR_GALLERY=2;

    @Override
    public void onCreate(Bundle savedInstasceState){
        super.onCreate(savedInstasceState);

        setContentView(R.layout.student);

        nomAlumne=getIntent().getExtras().getString("nom");
        nomAula=getIntent().getExtras().getString("nomAula");


        /////////////////////           //amb aquest metode també sagafen les notes del alumne
        obtenirDadesAlumne();  //metode per obtenir les dades del alumne del que extreurem informació
        /////////////////////

        TextView t=(TextView)findViewById(R.id.txtNomAlumne);
        t.setText(nomAlumne);

        avatar=findViewById(R.id.imageStudent);
        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPhotoDialog();
            }
        });

        TextView r=findViewById(R.id.txtRetards);
        r.append(""+dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).retards);

        TextView f=findViewById(R.id.txtFaltes);
        f.append(""+dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).faltes);
        //////////////////////////
        actualitzarNotaFinal();
        /////////////////////////


        DisplayMetrics dm=new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int widthTv=dm.widthPixels / 4;
        int heightTv=dm.heightPixels / 10;

        int widthTvn=dm.widthPixels / 7;
        int heightTvn=dm.heightPixels / 10;

        int widthTv3=dm.widthPixels / 4;
        int heightTv3=dm.heightPixels / 10;

        int widthEtv=dm.widthPixels / 7;
        int heightEtv=dm.heightPixels / 10;

        int widthEtp=dm.widthPixels / 4;
        int heightEtp=dm.heightPixels / 10;

        int widthb=dm.widthPixels / 5;
        int heightb=dm.heightPixels / 10;


        LinearLayout linearLayout=(LinearLayout)findViewById(R.id.layoutActivitatsEvaluacio);

        //linearLayout.removeAllViews();


            //Toast.makeText(this,dades.get(obtenirClasse()).alumnes.size(),Toast.LENGTH_LONG).show();  //probes per saber lerror


        for(int k=0;k<dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes.size();k++){



            TextView tv=new TextView(this);      ///nom AE
            tv.setText(dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes.get(k).nom);
            tv.setWidth(widthTv);
            tv.setHeight(heightTv);
            tv.setTextSize(12);
            tv.setTextColor( getResources().getColor(R.color.colorAccent));
            tv.invalidate();
                               //a la hora de modificar els valors amb aquest id es sabra la nota que s'ha de modificar

            TextView tv2=new TextView(this);      ///nom AE
            tv2.setText(" nota:");
            tv2.setWidth(widthTvn);
            tv2.setHeight(heightTvn);
            tv2.setTextSize(12);
            tv2.setTextColor( getResources().getColor(R.color.colorPrimaryDark));
            tv2.invalidate();

            EditText etv=new EditText(this);
            etv.setText(""+dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes.get(k).valor);
            tv.setWidth(widthEtv);
            tv.setHeight(heightEtv);
            etv.setId(k);
            etv.invalidate();

            TextView tv3=new TextView(this);      ///nom AE
            tv3.setText(" percentatge:");
            tv3.setWidth(widthTv3);
            tv3.setHeight(heightTv3);
            tv3.setTextSize(12);
            tv3.setTextColor( getResources().getColor(R.color.colorPrimaryDark));
            tv3.invalidate();

            EditText etp=new EditText(this);
            etp.setText(""+dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes.get(k).percentatge);
            tv.setWidth(widthEtp);
            //tv.setBackgroundResource(@android:drawable/editbox_background);
            tv.setHeight(heightEtp);
            etp.setId(k+50);                                       //per obtenir el index en aquest cas s'ha de restar per 50
            etp.invalidate();
/*
            Button b=new Button(this);
            b.setText("eliminar");                     //AL ELIMINAR UNA ACTIVITAT DEVALUACIO ELS CANVIA SAPLICARAN PER A TOTS ELS ALUMNES DE LA MATEIXA AULA
            tv.setWidth(widthb);                        //AL FINAL EL BOTO CREC QUE SAURIA DE TREURE
            tv.setHeight(heightb);
            etp.setId(k+70);                                       //per obtenir el index en aquest cas s'ha de restar per 70
            etp.invalidate();
*/
            LinearLayout l=new LinearLayout(this);


            l.setOrientation(LinearLayout.HORIZONTAL);
            l.addView(tv);
            l.addView(tv2);
            l.addView(etv);
            l.addView(tv3);
            l.addView(etp);
            //l.addView(b);


            linearLayout.addView(l);

        }





        FloatingActionButton options =(FloatingActionButton)findViewById(R.id.buttonOpcionsAlumne);
        options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerForContextMenu(view);
                openContextMenu(view);
            }
        });



        FloatingActionButton save=(FloatingActionButton)findViewById(R.id.buttonGuardarCanvisAlumne);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

             for(int i=0;i<dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes.size();i++){
                 EditText etv=(EditText)findViewById(i); //MIRAR
                 if(!etv.getText().toString().equals("")) {
                     dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes.get(i).valor = Float.parseFloat(etv.getText().toString()); //MIRAR
                 }

                 EditText etp=(EditText)findViewById(i+50); //MIRAR
                 if(!etp.getText().toString().equals("")) {
                     dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes.get(i).percentatge = Float.parseFloat(etp.getText().toString()); //MIRAR
                 }


             }

            // guardarDades(dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())));  //MIRAR
              guardarDades();
                actualitzarNotaFinal();
                }catch (Exception e){
                    toast("valor incorrecte");

                }

            }
        });


        ImageButton imageStudent=findViewById(R.id.imageStudent);
        imageStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                                     //posar intent per accedir a la camera o a la galeria
            }
        });



    }


    public void toast(String s){
        Toast.makeText(this,s,Toast.LENGTH_LONG).show();
    }


    public void actualitzarNotaFinal(){
        TextView t2=(TextView)findViewById(R.id.txtNotaFinal);
        dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).calcularNotaFinal();
        t2.setText("Nota final: "+dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notaFinal);
    }

//////////////////////////////////////////////////////////////////////////


    public void avatarPhoto(){
        Intent pictureIntent =new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(pictureIntent,ACTION_AVATAR_PHOTO);

    }

    public void avatarGallery(){
        Intent pictureIntent =new Intent(Intent.ACTION_PICK);
        pictureIntent.setType("image/*");
        startActivityForResult(pictureIntent,ACTION_AVATAR_GALLERY);

    }

    public void showPhotoDialog(){
        android.app.FragmentManager fragment=getFragmentManager();
        PhotoSelectorDialog dlg=new PhotoSelectorDialog();
        dlg.show(fragment,"photo"); //MIRAR
    }

    public void defaultAvatar(){
        /*
        SharedPreferences.Editor editor=preferences.edit();
        editor.remove(AVATAR);
        editor.commit();
        setAvatar();
        */

    }


//////////////////////////////////////////////////////////////////////////





    public void guardarDades(){

        OutputStreamWriter out;

        try{


            //dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes = a.notes;
            out=new OutputStreamWriter(openFileOutput("dades",0));


            String jsonObjetos = new Gson().toJson(dades);


            out.write(jsonObjetos);
            out.flush();
            out.close();
            Toast.makeText(this,"Guardant...",Toast.LENGTH_SHORT).show();


        }catch (Exception e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }

    }


    public void obtenirDadesAlumne(){ //METODE PER RECUPERAR L'ARRAY GUARDADA A LA MEMORIA DEL TELEFON

        DadesAlumne al=null;
        InputStreamReader in;

       try{
            in=new InputStreamReader(openFileInput("dades"));
            BufferedReader bufferedReader=new BufferedReader(in);
            StringBuffer info= new StringBuffer();
            String tmp=null;
            while((tmp=bufferedReader.readLine())!=null){
                info.append(tmp);
            }
            in.close();
            //  SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
            Gson gson=new Gson();
            //String objectes = prefs.getString("b","");
            //String json = new Gson().toJson(objectes);
            JSONArray jsonArray = new JSONArray(info.toString()); //MIrar si es pot sense aquesta linia
            Type listType = new TypeToken<ArrayList<DadesAula>>(){}.getType(); //crec que la versio importada de Type no és la correcta
            dades=new Gson().fromJson(jsonArray.toString(),listType); //json




            int g=0;
            for(int b=0;b<dades.size();b++){
                if(dades.get(b).name.equals(nomAula)){
                    g=b;
                    //notesAlumne= a.get(b).notes;   //NO FA FALTA
                }
            }
            for(int k=0;k<dades.get(g).alumnes.size();k++){
                if(dades.get(g).alumnes.get(k).nom.equals(nomAlumne)){
                    //al=a.get(g).alumnes.get(k);  //MIRAR

                }
            }


        }
        catch (Exception e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }



    }


    public int obtenirClasse(){
        int g=0;
        for(int b=0;b<dades.size();b++){
            if(dades.get(b).name.equals(nomAula)){
                g=b;

            }
        }
        return g;


    }
    public int obtenirAlumne(int g){
        int t=0;
        for(int k=0;k<dades.get(g).alumnes.size();k++){
            if(dades.get(g).alumnes.get(k).nom.equals(nomAlumne)){
               t=k;   //MIRAR

            }
        }

        return t;
    }

    public void eliminarAE(){
        Intent i=new Intent(this,EliminarAE.class);
        i.putExtra("nomAula",nomAula);
        i.putExtra("nomAlumne",nomAlumne);
        i.putExtra("id",1);
        startActivityForResult(i,ELIMINARAE);
    }



    public void afeguirAE(){
        Intent i=new Intent(this,AfeguirAE.class);
        i.putExtra("nomAula",nomAula);
        i.putExtra("nomAlumne",nomAlumne);
        i.putExtra("id",1);
        startActivityForResult(i,AFEGUIRAE);

    }



    public void actualitzaLayout(){
        LinearLayout linearLayout=(LinearLayout)findViewById(R.id.layoutActivitatsEvaluacio);
        linearLayout.removeAllViews();
        DisplayMetrics dm=new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int widthTv=dm.widthPixels / 9;
        int heightTv=dm.heightPixels / 10;

        int widthTv3=dm.widthPixels / 4;
        int heightTv3=dm.heightPixels / 10;

        int widthEtv=dm.widthPixels / 7;
        int heightEtv=dm.heightPixels / 10;

        int widthEtp=dm.widthPixels / 6;
        int heightEtp=dm.heightPixels / 10;

        int widthb=dm.widthPixels / 5;
        int heightb=dm.heightPixels / 10;
        for(int k=0;k<dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes.size();k++){



            TextView tv=new TextView(this);      ///nom AE
            tv.setText(dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes.get(k).nom);
            tv.setWidth(widthTv);
            tv.setHeight(heightTv);
            tv.invalidate();
            //a la hora de modificar els valors amb aquest id es sabra la nota que s'ha de modificar

            TextView tv2=new TextView(this);      ///nom AE
            tv2.setText("nota:");
            tv2.setWidth(widthTv);
            tv2.setHeight(heightTv);
            tv2.invalidate();

            EditText etv=new EditText(this);
            etv.setText(""+dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes.get(k).valor);
            tv.setWidth(widthEtv);
            tv.setHeight(heightEtv);
            etv.setId(k);
            etv.invalidate();

            TextView tv3=new TextView(this);      ///nom AE
            tv3.setText("percentatge:");
            tv3.setWidth(widthTv3);
            tv3.setHeight(heightTv3);
            tv3.invalidate();

            EditText etp=new EditText(this);
            etp.setText(""+dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes.get(k).percentatge);
            tv.setWidth(widthEtp);
            //tv.setBackgroundResource(@android:drawable/editbox_background);
            tv.setHeight(heightEtp);
            etp.setId(k+50);                                       //per obtenir el index en aquest cas s'ha de restar per 50
            etp.invalidate();
/*
            Button b=new Button(this);
            b.setText("eliminar");                     //AL ELIMINAR UNA ACTIVITAT DEVALUACIO ELS CANVIA SAPLICARAN PER A TOTS ELS ALUMNES DE LA MATEIXA AULA
            tv.setWidth(widthb);                        //AL FINAL EL BOTO CREC QUE SAURIA DE TREURE
            tv.setHeight(heightb);
            etp.setId(k+70);                                       //per obtenir el index en aquest cas s'ha de restar per 70
            etp.invalidate();
*/
            LinearLayout l=new LinearLayout(this);


            l.setOrientation(LinearLayout.HORIZONTAL);
            l.addView(tv);
            l.addView(tv2);
            l.addView(etv);
            l.addView(tv3);
            l.addView(etp);
            //l.addView(b);


            linearLayout.addView(l);

        }



    }




    @Override
    public void onActivityResult(int requestCode,int resultCode, Intent i){
        if(requestCode==AFEGUIRAE){
            if(resultCode==RESULT_OK){
                dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).notes.addAll((ArrayList<NotesAlumne>)i.getSerializableExtra("notes"));
                guardarDades();
                actualitzaLayout();

            }

        }
        if(requestCode==ELIMINARAE){
            if(resultCode==RESULT_OK){

                obtenirDadesAlumne();
                actualitzaLayout();

            }

        }
        if(requestCode==APUNTS){
            if(resultCode==RESULT_CANCELED){

                obtenirDadesAlumne();


            }

        }

        if(requestCode==CONTACTAR){
            if(resultCode==RESULT_CANCELED){

                obtenirDadesAlumne();


            }

        }

        if(requestCode==FRT){
            if(resultCode==RESULT_CANCELED){

                obtenirDadesAlumne();
                TextView r=findViewById(R.id.txtRetards);
                r.setText("retards: "+dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).retards);

                TextView f=findViewById(R.id.txtFaltes);
                f.setText("faltes: "+dades.get(obtenirClasse()).alumnes.get(obtenirAlumne(obtenirClasse())).faltes);


            }

        }

        if(requestCode==ACTION_AVATAR_GALLERY){
            if(resultCode==RESULT_OK){
                Uri photoUri=i.getData();
                try{
                    Bitmap gallery=MediaStore.Images.Media.getBitmap(getContentResolver(),photoUri);  //posant nomes media ja estaria be
                    saveAvatar(gallery);

                }catch(Exception e){
                    Toast.makeText(this,"Error: "+e.getLocalizedMessage(),Toast.LENGTH_LONG).show();
                }

            }

        }

        if(requestCode==ACTION_AVATAR_PHOTO){
            if(resultCode==RESULT_OK){
                Bitmap cameraPic=(Bitmap) i.getExtras().get("data"); //MIRAR SI ES POT FER AMB CUALSEVOL STRING
                saveAvatar(cameraPic);

            }

        }

    }


    public void saveAvatar(Bitmap newPhotoAvatar){
        avatar.setImageBitmap(newPhotoAvatar);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){  //MILLOR FER AMB UN FRAGMENT
        super.onCreateContextMenu(menu,v,menuInfo);
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_alumne,menu);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item){           //MILLOR FER AMB UN FRAGMENT



        switch (item.getItemId()){
            case R.id.contacteAlumne:

                contactarAlumne();

                return true;
            case R.id.apuntsAlumne:

                anarApuntsAlumne();
                return true;
            case R.id.faltesRetardsTargetes:

                faltesretardsTargetes();

                return true;
            case R.id.afeguirAEalumne:
                 afeguirAE();
                return true;
            case R.id.eliminarAEalumne:
                 eliminarAE();

                return true;
            default:
                return super.onContextItemSelected(item);

        }
    }


    public void anarApuntsAlumne(){
       Intent i=new Intent(this,AnotacionsAlumne.class);
       i.putExtra("nomAula",nomAula);
       i.putExtra("nomAlumne",nomAlumne);
       startActivityForResult(i,APUNTS);


    }

    public void contactarAlumne(){
        Intent i=new Intent(this,ContactarAlumne.class);
        i.putExtra("nomAula",nomAula);
        i.putExtra("nomAlumne",nomAlumne);
        startActivityForResult(i,CONTACTAR);


    }


    public void faltesretardsTargetes(){
        Intent i=new Intent(this,RetardsFaltesTargetes.class);
        i.putExtra("nomAula",nomAula);
        i.putExtra("nomAlumne",nomAlumne);
        startActivityForResult(i,FRT);
    }
}
