package ins.franjupucrack.franju.classroommanagement_v1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franju.classroommanagement_v1.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;


public class AddClassroom extends Activity {

    public final int ALUMNESNOVACLASSE=1;

    EditText entradaNom;
    LinearLayout tercerLayout;
    LinearLayout segonLayout;
    LinearLayout primerLayout;
    String nomAula="Aula";
    Button nomAulaOk;
    EditText entradapercentActEval;
    EditText entradaNomActEval;
    ArrayList<NotesAlumne> AE=new ArrayList<>();
    Button botoPerAE;

    ArrayList<DadesAula> aules=new ArrayList<>();

    Button botoPerAssistentAA;
    ////////////////////////////////////////////////////////////

    @Override
    public void onCreate(Bundle savedInstasceState){
        super.onCreate(savedInstasceState);


        setContentView(R.layout.add_classroom);

        //////////////////////
        recuperarDades();
        //////////////////////


        primerLayout=(LinearLayout)findViewById(R.id.layoutPrincipalAddClassroom);
        primerLayout.setVisibility(View.VISIBLE);//pot ser que no fagi falta
        ////////////////////////////////////////////////////////////////////////////////////////////
        segonLayout=(LinearLayout)findViewById(R.id.layoutPerAmagarAE); //segon layout
        segonLayout.setVisibility(View.GONE);

        tercerLayout=(LinearLayout)findViewById(R.id.layoutPerAmagarBotoPerAnarAsistentCA); //tercer layout
        tercerLayout.setVisibility(View.GONE);
        ///////////////////////////////////////////////////////////////////////////////////////////

        entradaNom=(EditText)findViewById(R.id.entradaTextNomAula);

        nomAulaOk=(Button)findViewById(R.id.botoOknomClasse);
        nomAulaOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                entradaNomActEval = (EditText) findViewById(R.id.entradaTextNomActivitatEvaluacio);
                entradapercentActEval = (EditText) findViewById(R.id.entradaTextPercentatgeActivitatEvaluacio);

                nomAula = entradaNom.getText().toString();

                if (nomCorrecte(nomAula)) {

                    primerLayout.setVisibility(View.GONE);
                    segonLayout.setVisibility(View.VISIBLE);
                    tercerLayout.setVisibility(View.GONE);

                    botoPerAE = (Button) findViewById(R.id.botoPerAE);
                    TextView txtNomAula = (TextView) findViewById(R.id.textViewnomAula);
                    txtNomAula.append(nomAula);

                    botoPerAssistentAA = (Button) findViewById(R.id.botoPerAnarAssistentPAA);
                    botoPerAE.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (nomAECorrecte(entradaNomActEval.getText().toString())) {
                                try {

                                afegirActivitatEvaluacio();
                                tercerLayout.setVisibility(View.VISIBLE);


                                botoPerAssistentAA.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {              //es canvia a l'activiti de l'assistent per afegir alumnes



                                        //intent
                                        afegirAlumnes();

                                        //aqui hi havia posat un finish pero crec que no era correcte

                                    }
                                });
                                }catch (Exception e){
                                    toast("valor incorrecte");

                                }

                            }else{
                                toast("Aquest nom ja està en us, escull un altre!");

                            }
                        }
                    });


                }else{


                    toast("Aquest nom ja està en us, escull un altre!");
                }
            }
        });



    }


    public boolean nomCorrecte(String s){
        boolean t=true;
        for(int i=0;i<aules.size();i++){

            if(aules.get(i).name.equals(s)){

                t=false;
            }


        }

        return t;
    }

    public boolean nomAECorrecte(String s){
        boolean t=true;

        for(int i=0;i<AE.size();i++){
            if(AE.get(i).nom.equals(s)){
                t=false;
            }

        }


        return t;



    }

    ////////////////////////////////////
        public void afegirActivitatEvaluacio(){


            NotesAlumne nAlumne=new NotesAlumne(entradaNomActEval.getText().toString(),Integer.valueOf(entradapercentActEval.getText().toString()));
            AE.add(nAlumne); //MIRAR
            toast("activitat d'evaluació afeguida");
            entradaNomActEval.setText("");
            entradapercentActEval.setText("");

        }


        public void afegirAlumnes(){

            Intent i=new Intent(this,AsistentAfegirAlumnes.class);
            //i.putExtra("notesAE",AE); //utilitzar metode fer obtenir aquest valor en lasistent de creacio d'alumnes //MIRAR PROBLEMA QUE NO ES PODEN POSAR SERIALIZABLES, ACCEDIR A LES NOTES PER OBJECTE BUNDLE O ALGO AIXÍ
            //i.putExtra("nomAula",nomAula);
            i.putExtra("id",0);
            i.putExtra("nomAula",nomAula);
            startActivityForResult(i,ALUMNESNOVACLASSE);

        }





        @Override
        protected void onActivityResult(int requestCode,int resultCode,Intent data){   //sobreescripcio del metode per actuar despres de rebre la llista amb els alumnes i comunicarla amb el main activity

            if(requestCode==ALUMNESNOVACLASSE){

                if(resultCode==RESULT_OK){

                    Bundle extras=getIntent().getExtras();
                    //ArrayList<DadesAlumne> alumnes=(ArrayList<DadesAlumne>)extras.getSerializable("Alumnes");

                    Intent resultIntent=new Intent((String)null);  //MIRAR
                    resultIntent.putExtra("Alumnes",data.getSerializableExtra("alumnes")); //MIRAR
                    resultIntent.putExtra("nomAula",nomAula);
                    resultIntent.putExtra("notesActivitatDevaluacio",AE);
                    setResult(RESULT_OK,resultIntent);
                    finish(); //retorn a la mainActivity

                }                                                                    //es posible que s'ahgi dafeguir el resultCanceled
            }



        }

    public void recuperarDades(){ //METODE PER RECUPERAR L'ARRAY GUARDADA A LA MEMORIA DEL TELEFON


        InputStreamReader in;

        try{
            in=new InputStreamReader(openFileInput("dades"));
            BufferedReader bufferedReader=new BufferedReader(in);
            StringBuffer info= new StringBuffer();
            String tmp=null;
            while((tmp=bufferedReader.readLine())!=null){
                info.append(tmp);
            }
            in.close();
            //  SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
            Gson gson=new Gson();
            //String objectes = prefs.getString("b","");
            //String json = new Gson().toJson(objectes);
            JSONArray jsonArray = new JSONArray(info.toString());
            Type listType = new TypeToken<ArrayList<DadesAula>>(){}.getType();
            aules=new Gson().fromJson(jsonArray.toString(),listType); //json



            TextView tv=(TextView)findViewById(R.id.textQueMostraLlistaBuida);


        }catch (Exception e){
            //Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }

    }




    ///////////////////////////////////////////////
   public void toast(String s){
    Toast.makeText(this,s,Toast.LENGTH_LONG).show();
}
}




