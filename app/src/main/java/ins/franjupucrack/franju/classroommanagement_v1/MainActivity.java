package ins.franjupucrack.franju.classroommanagement_v1;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franju.classroommanagement_v1.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public  class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {   //afeguir opcio per clonar una aula, pero amb un nom diferent


    private final int NOVA_CLASSE=1;

    private final int CAMBIAR_NOM_AULA=27523;

    private final int VISUALITZAR_AULA=2;
    private final int JUGAR=2635;

    public ArrayList<DadesAula> aules=new ArrayList<DadesAula>();     //Array amb tota les dades de l'aplicacio

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        ////////////////
        recuperarDades(); //AL INICIAR EL PROGRAMA ES COMPROBA
        ////////////////

        /////////////////////
        posarDadesALaLlista();        //AL INICIAR EL PROGRAMA SINICIA LADAPTER DE LA LISTVIEW AMB LES CLASSES CREADES
        //////////////////////


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                */

                crearAula();


            }
        });

        ListView listView = (ListView) findViewById(R.id.LlistaAules);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){                               //linies de codi per interectuar amb el pulsament d'un element de la llista
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                //super.onListItemClick(l, v, position, id);
                recuperarDades();
                // algo aixi pero cambiat
                //mirar si fa falta fer lo de mLastRowSelected
                ValorsLlista valorsLlista=(ValorsLlista) parent.getItemAtPosition(position);
                //TextView tv=(TextView)view.findViewById(R.id.textViewnomAula); //obtindre el nom de l'aula per saber de quina es tracta
                String n=valorsLlista.nomAula;//el nom de l'aula que se li pasa com a parametre al intent
                intentVisualitzarAula(n);       //METODE INTENT
                 //intentVisualitzarAula2(dadesAula); //mirar



            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View v=(View)findViewById(R.id.LlistaAules);   //MIRAR              //és el codig per fer que el context menu sobri quan es clica a un element de la llista
        registerForContextMenu(v);

    }


    /////////////////////////////////////////////////////////////////////////// METODES


/*
    public void guardarDades(){  //METODE PER GUARDAR L'ARRAY A LA MEMORIA DEL TELÈFON



        try{
            SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);

           // Integer i=10;

            String jsonObjetos = new Gson().toJson(aules);
            SharedPreferences.Editor editor=prefs.edit();
            editor.putString("b",jsonObjetos);//jsonObjetos
            //editor.clear(); ///UTILITZAR?
            editor.apply();

        }catch (Exception e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }

    }


    public void recuperarDades(){ //METODE PER RECUPERAR L'ARRAY GUARDADA A LA MEMORIA DEL TELEFON

      try{
            SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
            Gson gson=new Gson();
            String objectes = prefs.getString("b","");
            //String json = new Gson().toJson(objectes);
            JSONArray jsonArray = new JSONArray(objectes); //MIrar si es pot sense aquesta linia
            Type listType = new TypeToken<ArrayList<DadesAula>>(){}.getType(); //crec que la versio importada de Type no és la correcta
            aules=new Gson().fromJson(jsonArray.toString(),listType); //json
            TextView tv=(TextView)findViewById(R.id.textQueMostraLlistaBuida);


        }catch (Exception e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }

    }

*/
public void guardarDades(){  //METODE PER GUARDAR L'ARRAY A LA MEMORIA DEL TELÈFON

    OutputStreamWriter out;

    try{
       // SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);

        out=new OutputStreamWriter(openFileOutput("dades",0));


        String jsonObjetos = new Gson().toJson(aules);
        //SharedPreferences.Editor editor=prefs.edit();
       // editor.putString("b",jsonObjetos);//jsonObjetos

        out.write(jsonObjetos);
        out.flush();
        out.close();
        Toast.makeText(this,"Guardant...",Toast.LENGTH_SHORT).show();
        //editor.clear(); ///UTILITZAR?
        //editor.apply();

    }catch (Exception e){
       // Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

    }

}


    public void recuperarDades(){ //METODE PER RECUPERAR L'ARRAY GUARDADA A LA MEMORIA DEL TELEFON


        InputStreamReader in;

        try{
            in=new InputStreamReader(openFileInput("dades"));
            BufferedReader bufferedReader=new BufferedReader(in);
            StringBuffer info= new StringBuffer();
            String tmp=null;
            while((tmp=bufferedReader.readLine())!=null){
                info.append(tmp);
            }
            in.close();
          //  SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
            Gson gson=new Gson();
            //String objectes = prefs.getString("b","");
            //String json = new Gson().toJson(objectes);
            JSONArray jsonArray = new JSONArray(info.toString());
            Type listType = new TypeToken<ArrayList<DadesAula>>(){}.getType();
            aules=new Gson().fromJson(jsonArray.toString(),listType); //json



            TextView tv=(TextView)findViewById(R.id.textQueMostraLlistaBuida);


        }catch (Exception e){
           // Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }

    }



    public void intentVisualitzarAula(String n){ //metode per dirigir-se a l'activity per mostrar els alumnes i la info de la aula, PRIMERA MANERA DE FER-HO
       // recuperarDades();
         Intent i=new Intent(this,Classroom.class);
         DadesAula dadesAula=null;
         for(int a=0;a<aules.size();a++){
             if(aules.get(a).name.equals(n)){
                 dadesAula=aules.get(a);
             }
         }
            // i.putExtra("context",MainActivity.class);
             i.putExtra("nomDeLaula",n);
           //  i.putExtra("totesLesDades",aules);
            // i.putExtra("infoAula", dadesAula);//el que es fa es trobarel objecte classe que correspongi al seleccionat i enviarlo a lactivity corresponent
             startActivityForResult(i, VISUALITZAR_AULA);  //es retornara les posibles modificacions que se li hagin fet a la classe


         }
    public void intentVisualitzarAula2(DadesAula dadesAula){ //metode per dirigir-se a l'activity per mostrar els alumnes i la info de la aula, SEGONA MANERA DE FER-HO
        Intent i=new Intent(this,Classroom.class);           //ACTUALMENT NO ÉS FUNCIONAL

        i.putExtra("infoAula", dadesAula);                                                  //el que es fa es trobarel objecte classe que correspongi al seleccionat i enviarlo a lactivity corresponent
        startActivityForResult(i, VISUALITZAR_AULA);  //es retornara les posibles modificacions que se li hagin fet a la classe


    }




    public void crearAula(){                //metode per diriguirse a les activitats per la creaccio de una nova aula
        Intent i =new Intent(this,AddClassroom.class);

        startActivityForResult(i,NOVA_CLASSE);


    }


    public void afeguirAula(String name,ArrayList<DadesAlumne> alumnes,ArrayList<NotesAlumne> notesAE){   //metode per afeguir les dades de la nova aula a l'arrai principal

        DadesAula aula=new DadesAula(name,alumnes,notesAE);
        aules.add(aula);


    }


    public void posarDadesALaLlista() {   //metode per fer que la llista de les classes


        ArrayList<ValorsLlista> resultList = new ArrayList<ValorsLlista>();

        for(int i=0;i<aules.size();i++) {                  //AQUEST BUCLE OMPLE LES DADES DE LARRAY RESULTLIST

            ValorsLlista vl = new ValorsLlista();
            vl.nomAula = aules.get(i).name;
            resultList.add(vl);

        }

        Adaptadorllista adaptador = new Adaptadorllista(this, R.layout.layout_per_list_adapter, resultList, getLayoutInflater());

        ListView listView = (ListView) findViewById(R.id.LlistaAules);
        listView.setAdapter(adaptador);                 //ADAPTADOR DE LA LLISTA
        //setListAdapter(adaptador);
        if (aules.isEmpty()) {

            TextView tv = (TextView) findViewById(R.id.textQueMostraLlistaBuida);
            tv.setText("NO hi han classes creades");

        } else {
            TextView tv = (TextView) findViewById(R.id.textQueMostraLlistaBuida);
            tv.setText("");                                                             //MIRAR SI ES MILLOR POSAR UN HIDE
        }



    }


    public void eliminarClasse(String n){  //metode per borrar aula
        for(int i=0;i<aules.size();i++){
            if(aules.get(i).name.equals(n)){
                aules.remove(i);
            }

        }
        guardarDades();
        posarDadesALaLlista(); //actualitzarla
    }




    /////////////////////////////////////////////////////////////////////////// METODES



    /////////////////////////////////////////////////////////////////////////  CLASSES LOCALS QUE HEREDEN





    public static class ValorsLlista{ //classe per insertar els valors al contructor de l'array adapter

        public String nomAula;                       //afeguir mes parametres si més endevant es necesiten


    }



     public static class Adaptadorllista extends ArrayAdapter{               //classe que hereda de arrayAdapter i que permet que la listview mostri el contingut d'una array
         private LayoutInflater mInflater;
         private List<ValorsLlista> mObjects;

         public Adaptadorllista(Context context, int resource, List<ValorsLlista> objects, LayoutInflater inflater){                    //FER AIXOO
             super(context,resource,objects);
             mInflater=inflater;
             mObjects=objects;

         }

         @Override
         public View getView(int position, View convertView, ViewGroup parent){
             ValorsLlista valorsLlista=mObjects.get(position);
             View row=mInflater.inflate(R.layout.layout_per_list_adapter,null,false);

             TextView tv=(TextView) row.findViewById(R.id.textPerLayoutDeArrayAdapter);
             tv.setText(valorsLlista.nomAula); //se li assigna el nom de l'aula al text view que es mostrara a la llista d'aules

             return row;

         }


     }











    /////////////////////////////////////////////////////////////////////////  CLASSES LOCALS QUE HEREDEN






    ///////////////////////////////////////////////////////////////////////// METODES SOBREESCRITS





    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data){   //metode sobreescrit que afeguira l'aula

        if(requestCode==NOVA_CLASSE){
            if(resultCode==RESULT_OK){
                Bundle extras=getIntent().getExtras();
                afeguirAula(data.getStringExtra("nomAula"),(ArrayList<DadesAlumne>)data.getSerializableExtra("Alumnes"),(ArrayList<NotesAlumne>)data.getSerializableExtra("notesActivitatDevaluacio"));
                // proba utilitzant un textview per veure si els valors del bundle funcionen


                //Guardar g=new Guardar(this); //sistema per guardar utilitzant la classe Guardar



                guardarDades(); //MIRAR
                TextView tv=(TextView)findViewById(R.id.textQueMostraLlistaBuida);
                //tv.setText(data.getStringExtra("nomAula"));

            }
       }
       if(requestCode==VISUALITZAR_AULA){  //posar que fer quan es retorni a aquesta activity desde l'activity, pero intentar buscar manera de fer que es pugui editar l'arrai desde altres classes

           recuperarDades();

       }


        posarDadesALaLlista();  ///actualitzar la llista d'aules creades

        if(requestCode==CAMBIAR_NOM_AULA){
            if(resultCode==RESULT_OK){
                recuperarDades();
                posarDadesALaLlista();

            }

        }

        if(requestCode==JUGAR){
            if(resultCode==RESULT_OK){

                recuperarDades();

                //ACCIONS A FER QUAN SACABI LA PARTIDA (ES POSIBLE QUE NO ES FAGUI CAP ACCIO)

            }

        }

    }






    @Override
    public void onCreateContextMenu(ContextMenu menu,View v,ContextMenu.ContextMenuInfo menuInfo){  //menu per cuan mantens pulsat un element de la llista
        super.onCreateContextMenu(menu,v,menuInfo);
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.clicar_element_llista,menu);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        AdapterView.AdapterContextMenuInfo delw=(AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        //mLastRowSelected=delw.position; -> mirar si es necesari

        final String n=aules.get(delw.position).name; //MIRAR , OBTENIR EL NOM DE LAULA PER SABER DE QUINA ES TRACTA, MIRAR SI ES MILLOR UTILITZAR UN ID
        switch (item.getItemId()){
            case R.id.eliminarClasseMenuLlista:
                new AlertDialog.Builder(this).setTitle("segur que vols eliminar la classe?").setPositiveButton("si",new AlertDialog.OnClickListener(){

                    public void onClick(DialogInterface dg, int i){
                        eliminarClasse(n);
                    }
                }).setNegativeButton("no",null).show();
                return true;
            case R.id.modificarNomAula:

                  modificarNomAula(n);
                  return true;

            case R.id.jugarMemoriClasseMenuLlista:

                jugarPartida(n);

                return true;
            case R.id.clonarLaAula:

                clonarAula(delw.position);

                return true;
            default:
                return super.onContextItemSelected(item);

        }
    }



    public void clonarAula(int i){
        ArrayList<DadesAula> d=(ArrayList<DadesAula>) aules.clone();
      DadesAula d2=new DadesAula(d.get(i).name+"_copia",d.get(i).alumnes,d.get(i).notes);


      //d2.name=d2.name+"2";
      aules.add(d2);
      guardarDades();
      posarDadesALaLlista();

    }



    public void jugarPartida(String n){
        Intent i=new Intent(this,Memori.class);
        i.putExtra("nomAula",n);
        startActivityForResult(i,JUGAR);

    }

    public void modificarNomAula(String n){
        Intent i=new Intent(this,CambiarNomAula.class);
        i.putExtra("nomAula",n);
        startActivityForResult(i,CAMBIAR_NOM_AULA);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) { //metode per accionar la accio corresponent segons l'item que agis clicat dins del drawer layout
        // Handle navigation view item clicks here.
        int id = item.getItemId();

     if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    ///////////////////////////////////////////////////////////////////////// METODES SOBREESCRITS
}
