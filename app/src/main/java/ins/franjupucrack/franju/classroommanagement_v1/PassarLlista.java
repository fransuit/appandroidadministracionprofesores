package ins.franjupucrack.franju.classroommanagement_v1;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franju.classroommanagement_v1.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;


public class PassarLlista extends Activity {



    public ArrayList<DadesAula> dades=new ArrayList<DadesAula>();
    String nomAula;


    @Override
    public void onCreate(Bundle savedInstasceState) {
        super.onCreate(savedInstasceState);

        setContentView(R.layout.pasar_llista);

        //////////////////////
        recuperarDades();
        //////////////////////
        nomAula=getIntent().getExtras().getString("nomAula");


        ////////////////////////////////////////////////////////////////////////////

        DisplayMetrics dm=new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int widthTv=dm.widthPixels / 6;
        int heightTv=dm.heightPixels / 10;

        int widthB=dm.widthPixels / 20;
        int heightB=dm.heightPixels / 20;


        LinearLayout LinearLayout=findViewById(R.id.containerPasarLlista);

        for(int i=0;i<dades.get(obtenirClasse()).alumnes.size();i++){

            TextView tv=new TextView(this);      ///nom AE
            tv.setText(dades.get(obtenirClasse()).alumnes.get(i).nom);
            tv.setWidth(widthTv);
            tv.setHeight(heightTv);
            tv.invalidate();


                                                                //una idea pot ser sobreescriure el metode Button i afeguirli informacio per poder saber si s'ha clicat o no
//////////////////////////////////////////////////////////////////////////
             BotoPasarLlista asistencia=new BotoPasarLlista(this,1);
             asistencia.setWidth(widthB);
             asistencia.setHeight(heightB);
             asistencia.setId(i+700);                               //HI POT AVER PROBLEMES SI HI HA 2 ID IGUALS EN EL PROGRAMA
             asistencia.setBackgroundColor(asistencia.fons());
             asistencia.setText("A");

//////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////
            BotoPasarLlista retard=new BotoPasarLlista(this,2);
            retard.setWidth(widthB);
            retard.setHeight(heightB);
            retard.setId(i+900);
            retard.setBackgroundColor(retard.fons());
            retard.setText("R");

///////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////
            BotoPasarLlista falta=new BotoPasarLlista(this,3);
            falta.setWidth(widthB);
            falta.setHeight(heightB);
            falta.setId(i+990);
            falta.setBackgroundColor(falta.fons());
            falta.setText("F");

////////////////////////////////////////////////////////////////////////

            LinearLayout l=new LinearLayout(this);
            l.setOrientation(LinearLayout.HORIZONTAL);
            l.addView(tv);
            l.addView(asistencia);
            l.addView(retard);
            l.addView(falta);


            LinearLayout.addView(l);
        }

        /////////////////////////////////////////////////////////////////////////////////
        for(int i=0;i<dades.get(obtenirClasse()).alumnes.size();i++){
            asignarFuncioBotoA(i+700);
            asignarFuncioBotoR(i+900);
            asignarFuncioBotoF(i+990);
        }



        Button pasarLlista=findViewById(R.id.buttonPerPassarLlista);
        pasarLlista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                tornar();



            }
        });
    }





    public void asignarFuncioBotoA(int id){
        final BotoPasarLlista a=findViewById(id);
        final BotoPasarLlista r=findViewById(id+200);
        final BotoPasarLlista f=findViewById(id+290);
        final int d=id;
        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                  a.clicat=true;
                  a.setBackgroundColor(a.fons());
                  a.invalidate();
                  r.clicat=false;
                  r.setBackgroundColor(r.fons());
                  r.invalidate();
                  f.clicat=false;
                  f.setBackgroundColor(f.fons());
                  f.invalidate();

            }
        });
    }
    public void asignarFuncioBotoR(int id){
        final BotoPasarLlista r=findViewById(id);
        final BotoPasarLlista a=findViewById(id-200);
        final BotoPasarLlista f=findViewById(id+90);
        final int d=id;
        r.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                a.clicat=false;
                a.setBackgroundColor(a.fons());
                a.invalidate();
                r.clicat=true;
                r.setBackgroundColor(r.fons());
                r.invalidate();
                f.clicat=false;
                f.setBackgroundColor(f.fons());
                f.invalidate();

            }
        });
    }

    public void asignarFuncioBotoF(int id){
        final BotoPasarLlista r=findViewById(id-90);
        final BotoPasarLlista a=findViewById(id-290);
        final BotoPasarLlista f=findViewById(id);
        final int d=id;
        f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                a.clicat=false;
                a.setBackgroundColor(a.fons());
                a.invalidate();
                r.clicat=false;
                r.setBackgroundColor(r.fons());
                r.invalidate();
                f.clicat=true;
                f.setBackgroundColor(f.fons());
                f.invalidate();

            }
        });
    }





    public void tornar(){
        //obtenir les faltes i retards dels alumnes  i retornar lactivitat
        for(int i=0;i<dades.get(obtenirClasse()).alumnes.size();i++){
            BotoPasarLlista a=findViewById(i+700);
            BotoPasarLlista r=findViewById(i+900);
            BotoPasarLlista f=findViewById(i+990);

            if(a.clicat){
                dades.get(obtenirClasse()).alumnes.get(i).asistencies++; //potse es inutil
            }
            if(r.clicat){
                dades.get(obtenirClasse()).alumnes.get(i).retards++;
            }
            if(f.clicat){
                dades.get(obtenirClasse()).alumnes.get(i).faltes++;
            }

        }

        guardarCanvis();

        setResult(RESULT_OK);
        finish();


    }



    public void recuperarDades(){ //METODE PER RECUPERAR L'ARRAY GUARDADA A LA MEMORIA DEL TELEFON


        InputStreamReader in;

        try{
            in=new InputStreamReader(openFileInput("dades"));
            BufferedReader bufferedReader=new BufferedReader(in);
            StringBuffer info= new StringBuffer();
            String tmp=null;
            while((tmp=bufferedReader.readLine())!=null){
                info.append(tmp);
            }
            in.close();
            //  SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
            Gson gson=new Gson();
            //String objectes = prefs.getString("b","");
            //String json = new Gson().toJson(objectes);
            JSONArray jsonArray = new JSONArray(info.toString()); //MIrar si es pot sense aquesta linia
            Type listType = new TypeToken<ArrayList<DadesAula>>(){}.getType(); //crec que la versio importada de Type no és la correcta
            dades=new Gson().fromJson(jsonArray.toString(),listType); //json






        }catch (Exception e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }

    }
    public void guardarCanvis(){

        OutputStreamWriter out;

        try{


            out=new OutputStreamWriter(openFileOutput("dades",0));


            String jsonObjetos = new Gson().toJson(dades);

            out.write(jsonObjetos);
            out.flush();
            out.close();
            Toast.makeText(this,"Guardant...",Toast.LENGTH_SHORT).show();


        }catch (Exception e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();

        }
    }

    public int obtenirClasse(){
        int g=0;
        for(int b=0;b<dades.size();b++){
            if(dades.get(b).name.equals(nomAula)){
                g=b;

            }
        }
        return g;


    }


}
